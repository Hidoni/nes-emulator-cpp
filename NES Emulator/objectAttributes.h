#pragma once
#include <bitset>

struct ObjectAttributes
{
	std::bitset<8> yPosition; // Top Left Y Position of sprite
	std::bitset<8> tileIndex; // bit 0 is the pattern table used, rest are indexes for the sprite.
	std::bitset<8> objectAttributes; // Palette, priority and whether the sprite should be flipped.
	std::bitset<8> xPosition; // Top Left X Position of sprite

	ObjectAttributes() {};
	ObjectAttributes(uint8_t yPosition, uint8_t tileIndex, uint8_t objectAttributes, uint8_t xPosition) : yPosition(yPosition), tileIndex(tileIndex), objectAttributes(objectAttributes), xPosition(xPosition) {}
};