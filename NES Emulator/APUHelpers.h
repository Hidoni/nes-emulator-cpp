#pragma once
#include <cstdint>
#include <vector>

class LengthCounter
{
private:
	const std::vector<size_t> m_lookupTable = { 10, 254, 20, 2, 40, 4, 80, 6, 160, 8, 60, 10, 14, 12, 26, 14,
			12, 16, 24, 18, 48, 20, 96, 22, 192, 24, 72, 26, 16, 28, 32, 30 };
	bool m_enabled;
	bool m_halt;
	size_t m_counter;

public:
	LengthCounter();
	void setHalt(bool halt);
	void setEnabled(bool enabled);
	void setCounter(size_t index);
	size_t getCounter();
	bool disableChannel();

	void clock(); // Same as tick.
};

class Divider
{
	size_t m_period;
	size_t m_counter;
public:
	Divider();
	size_t getPeriod();
	size_t getCounter();
	void setPeriod(size_t period);
	void resetCounter();
	bool clock();
};

class Envelope // The Pulse and Noise channels have envelope units.
{
	bool m_restart;
	bool m_counterLoopEnabled;
	Divider m_divider;
	size_t m_counter; // Envelope Volume Value if not set at constant volume mode
	bool m_constantVolumeEnabled;
	size_t m_constantVolume; // Divider is loaded with this value.
public:
	Envelope();
	void restartEnvelope();
	void setCounterLoopEnabled(bool enable);
	void setConstantVolumeEnabled(bool enable);
	void setConstantVolume(size_t volume);
	size_t getVolume();
	void clock();
};

class Timer
{
	Divider m_divider;
	size_t m_minimumPeriod;
public:
	Timer();
	void reset();
	size_t getDividerPeriod();
	void setDividerPeriod(size_t period);
	void setLowBitsOfDividerPeriod(size_t value);
	void setHighBitsOfDividerPeriod(size_t value);
	void setMinimumPeriod(size_t period);
	bool clock();
};

class SweepUnit
{
	bool m_adjustForPulseOne;
	bool m_enabled;
	bool m_negate;
	bool m_reload;
	bool m_disableChannel;
	size_t m_shiftAmount;
	Divider m_divider;
	size_t m_targetPeriod;

	void calculateTargetPeriod(Timer& timer);
	void adjustTimerDividerPeriod(Timer& timer);


public:
	SweepUnit(bool pulseOne);
	void setEnabled(bool enabled);
	void setNegate(bool negate);
	void setDividerPeriod(size_t period, Timer& timer);
	void setShiftAmount(size_t amount);
	void reload();
	bool channelDisabled();
	void clock(Timer& timer);
};

class LinearCounter
{
	bool m_reload;
	bool m_control;
	Divider m_divider;
public:
	LinearCounter();
	void reload();
	void handleWrite(size_t value);
	void clock();
	size_t getDividerCounter();
	bool disableChannel();
};

class PulseWaveGenerator
{
#ifdef USE_NEW_VALUES
	const std::vector<std::vector<uint8_t>> m_sequenceValues = {
				{ 0, 0, 0, 0, 0, 0, 0, 1 },
				{ 0, 0, 0, 0, 0, 0, 1, 1 },
				{ 0, 0, 0, 0, 1, 1, 1, 1 },
				{ 1, 1, 1, 1, 1, 1, 0, 0 } };
#else
	const std::vector<std::vector<size_t>> m_sequenceValues = {
				{ 0, 1, 0, 0, 0, 0, 0, 0 },
				{ 0, 1, 1, 0, 0, 0, 0, 0 },
				{ 0, 1, 1, 1, 1, 0, 0, 0 },
				{ 1, 0, 0, 1, 1, 1, 1, 1 } };
#endif
	size_t m_duty, m_step;
public:
	PulseWaveGenerator();
	void restart();
	void setDuty(size_t duty);
	size_t getValue();
	void clock();
};

class TriangleWaveGenerator
{
private:
	size_t m_step;
	const std::vector<size_t> m_sequence = { 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0,
											0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
public:
	TriangleWaveGenerator();
	size_t getValue();
	void clock();
};

class LinearFeedbackShiftRegister
{
private:
	size_t m_register;
	bool m_mode;
public:

	LinearFeedbackShiftRegister();
	void setMode(bool mode);
	bool disableChannel();
	void clock();
};