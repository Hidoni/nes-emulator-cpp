#include "SDL.h"
#include "CPU.h"
#include "PPU.h"
#include "APU.h"
#include "Cartridge.h"
#include "interruptFlags.h"
#include "InputCollector.h"
#include <cstdlib>
#include <time.h>
#include <iostream>
#include <vector>
#include <chrono>
#include <thread>

#define MASTER_CLOCK_DEFINITION (236.25 / 11)
#define MHZ_TO_HZ_RATIO 1000000
#define CPU_CLOCK_RATIO 12
#define WINDOW_HEIGHT 1024
#define WINDOW_WIDTH 960
#define SAMPLE_RATE 44100
#define SAMPLE_COUNT 4096

int main(int argc, char* argv[])
{
	const double masterClock = MASTER_CLOCK_DEFINITION * MHZ_TO_HZ_RATIO, cpuClock = masterClock / CPU_CLOCK_RATIO;
	bool endRun = false;
	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);
	SDL_Window* window;
	SDL_Renderer* renderer;
	SDL_Event evt;
	Cartridge* cart;
	SDL_AudioDeviceID audioDevice;
	if (argc == 2)
	{
		cart = new Cartridge(argv[1]);
		window = SDL_CreateWindow(argv[1], SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_HEIGHT, WINDOW_WIDTH, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
	}
	else
	{
		std::string romPath;
		std::cout << "No File Path Provided! Please Enter Path: ";
		std::getline(std::cin, romPath);
		cart = new Cartridge(romPath);
		window = SDL_CreateWindow(romPath.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_HEIGHT, WINDOW_WIDTH, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
	}
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC);
	SDL_AudioSpec want;
	want.freq = SAMPLE_RATE;
	want.format = AUDIO_F32SYS;
	want.channels = 1;
	want.silence = 0;
	want.samples = SAMPLE_COUNT;
	want.size = 0;
	want.callback = nullptr;
	audioDevice = SDL_OpenAudioDevice(SDL_GetAudioDeviceName(0, 0), 0, &want, nullptr, 0);
	const uint8_t* keyboard = SDL_GetKeyboardState(NULL);
	InputCollector controllerOne(keyboard), controllerTwo(keyboard);
	APUMemoryBus APUBus(cart);
	PPUMemoryBus PPUBus(cart);
	CPUMemoryBus CPUBus(cart, APUBus, PPUBus, controllerOne, controllerTwo);
	CPU cpu(CPUBus);
	PPU* ppu = new PPU(PPUBus, renderer); // Unfortunately the PPU must be created with a pointer, because of the next call:
	PPUBus.setPPU(ppu);
	APU* apu = new APU(APUBus, SAMPLE_RATE, audioDevice);
	APUBus.setAPU(apu);
	SDL_PauseAudioDevice(audioDevice, 0);
	while (!endRun)
	{
		int cyclesPassed = 0;
		auto startTime = std::chrono::system_clock::now();
		while (cyclesPassed < cpuClock && !endRun)
		{
			if (cyclesPassed % 100 == 0)
			{
				while (SDL_PollEvent(&evt))
				{
					switch (evt.type)
					{
					case SDL_KEYDOWN:
						switch (evt.key.keysym.scancode)
						{
#ifdef _DEBUG
						case SDL_Scancode::SDL_SCANCODE_F1:
							cpu.printDebug = !cpu.printDebug;
							break;
#endif
						case SDL_Scancode::SDL_SCANCODE_F2:
							SDL_SetWindowSize(window, 1024, 960);
							break;
						}
						break;
					case SDL_QUIT:
						endRun = true;
						break;
					}
				}
			}
			ppu->exec();
			ppu->exec();
			ppu->exec();
			apu->exec();
			cpu.exec_instruction();
			cyclesPassed++;
		}
		std::this_thread::sleep_until(startTime + std::chrono::seconds(1));
		std::chrono::duration<double> total = std::chrono::system_clock::now() - startTime;
#ifdef _DEBUG
		std::cout << "Executed " << cyclesPassed << " cycles in " << total.count() << std::endl;
#endif
	}
	delete ppu;
	delete cart;
	delete apu;
	return 0;
}