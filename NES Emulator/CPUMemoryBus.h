#pragma once
#include "Cartridge.h"
#include "PPUMemoryBus.h"
#include "APUMemoryBus.h"
#include "InputCollector.h"

class CPUMemoryBus
{
protected:
	std::vector<uint8_t> m_internal;
	Cartridge* m_cartridge;
	PPUMemoryBus& m_ppuBus;
	APUMemoryBus& m_apuBus;
	InputCollector& m_controllerOne, & m_controllerTwo;
public:
	CPUMemoryBus(Cartridge* cartridge, APUMemoryBus& apu, PPUMemoryBus& ppu, InputCollector& one, InputCollector& two);
	uint8_t read(uint16_t addr);
	void write(uint16_t addr, uint8_t val);
	void DMAWrite(std::array<uint8_t, 0x100> data);
#ifdef _DEBUG
	uint8_t peek(uint16_t addr) const;
#endif
};