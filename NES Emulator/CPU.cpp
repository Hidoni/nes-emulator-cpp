#include "CPU.h"
#include "interruptFlags.h"
#ifdef _DEBUG
#include <iomanip>
#include <iostream>
#include <sstream>
#define HEX_FORMAT std::setfill('0') << std::setw(2) << std::hex << std::uppercase
#endif
#define RESET_VECTOR_HIGHER 0xFFFD
#define RESET_VECTOR_LOWER 0xFFFC
#define OAMDMA_WRITE_CYCLES 513

CPU::CPU(CPUMemoryBus& bus) : m_bus(bus), currentInstruction(), m_inactiveCycles(0), targetAddr(0), targetAddrZeroPage(0), targetValue(0)
{
	// Initialize the Program Counter:
	uint16_t resetVectorPC = m_bus.read(RESET_VECTOR_LOWER); // Read the first 8 bits then shift the next half left by 8
	resetVectorPC += m_bus.read(RESET_VECTOR_HIGHER) << 8; 
	m_cpuRegisters.programCounter = resetVectorPC;
	// Initialize the "Always On" bit of the processor flags register:
	m_cpuRegisters.processorFlags[FlagAccessors::alwaysOne] = 1;
	globalCounters::CPUCycleCount = 0;
}

void CPU::fetchAddr(AddressingModes mode)
{
	targetValue = 0;
	targetAddr = 0;
	targetAddrZeroPage = 0;
	uint8_t argAddrZeroPage;
	uint16_t argAddr;
	uint16_t pc = m_cpuRegisters.programCounter.to_ulong();
	switch (mode)
	{
	case AddressingModes::relative: // As it turns out, these modes do the same thing, they only have differences in hardware and not in effect.
	case AddressingModes::immediate:
		targetAddr = pc++;
		break;
	
	case AddressingModes::zeroPage:
		targetAddrZeroPage = m_bus.read(pc++);
		targetAddr = targetAddrZeroPage;
		break;
	
	case AddressingModes::absolute:
		targetAddr = m_bus.read(pc++);
		targetAddr += m_bus.read(pc++) << 8;
		break;
	case AddressingModes::implied:
	case AddressingModes::accumulator:
		break;
	case AddressingModes::indexedX:
		if (pc >> 4 != (pc + 1) >> 4)
		{
			m_inactiveCycles++;
		}
		targetAddr = m_bus.read(pc++);
		if (pc >> 4 != (pc + 1) >> 4)
		{
			m_inactiveCycles++;
		}
		targetAddr += m_bus.read(pc++) << 8;
		targetAddr += m_cpuRegisters.xIndex.to_ulong();
		break;
	case AddressingModes::indexedY:
		if (pc >> 4 != (pc + 1) >> 4)
		{
			m_inactiveCycles++;
		}
		targetAddr = m_bus.read(pc++);
		if (pc >> 4 != (pc + 1) >> 4)
		{
			m_inactiveCycles++;
		}
		targetAddr += m_bus.read(pc++) << 8;
		targetAddr += m_cpuRegisters.yIndex.to_ulong();
		break;
	case AddressingModes::zeroPageIndexedX:
		targetAddrZeroPage = m_bus.read(pc++);
		targetAddrZeroPage += m_cpuRegisters.xIndex.to_ulong();
		targetAddr = targetAddrZeroPage;
		break;
	case AddressingModes::zeroPageIndexedY:
		targetAddrZeroPage = m_bus.read(pc++);
		targetAddrZeroPage += m_cpuRegisters.yIndex.to_ulong();
		targetAddr = targetAddrZeroPage;
		break;
	case AddressingModes::indirect:
		argAddr = m_bus.read(pc++);
		argAddr += m_bus.read(pc++) << 8;
		targetAddr = m_bus.read(argAddr);
		if ((argAddr & 0xFF) == 0xFF)
		{
			argAddr -= 0xFF;
		}
		else
		{
			argAddr++;
		}
		targetAddr += m_bus.read(argAddr) << 8;
		break;
	case AddressingModes::preIndexedIndirect:
		argAddrZeroPage = m_bus.read(pc++) + m_cpuRegisters.xIndex.to_ulong();
		targetAddr = m_bus.read(argAddrZeroPage++);
		targetAddr += m_bus.read(argAddrZeroPage) << 8;
		break;
	case AddressingModes::postIndexedIndirect:
		argAddrZeroPage = m_bus.read(pc++);
		targetAddr = m_bus.read(argAddrZeroPage++);
		targetAddr += m_bus.read(argAddrZeroPage) << 8;
		if (targetAddr >> 4 != (targetAddr + m_cpuRegisters.yIndex.to_ulong()) >> 4)
		{
			m_inactiveCycles++;
		}
		targetAddr += m_cpuRegisters.yIndex.to_ulong();
		break;
	}
	m_cpuRegisters.programCounter = pc;
}

void CPU::fetchValue()
{
	if (currentInstruction.mode != AddressingModes::accumulator && currentInstruction.mode != AddressingModes::implied)
	{
		targetValue = m_bus.read(targetAddr);
	}
	else
	{
		targetValue = m_cpuRegisters.accumulator.to_ulong();
	}
}

void CPU::exec_instruction()
{
	if (m_inactiveCycles == 0)
	{
		if (interruptFlags::NMI)
		{
			uint16_t pc = m_cpuRegisters.programCounter.to_ulong();
			push_stack(pc >> 8);
			push_stack(pc);
			push_stack(m_cpuRegisters.processorFlags.to_ulong());
			uint16_t interruptLocation = m_bus.read(0xFFFA);
			interruptLocation += m_bus.read(0xFFFB) << 8;
			m_cpuRegisters.programCounter = interruptLocation;
			interruptFlags::NMI = false;
		}
		if (interruptFlags::IRQ && !m_cpuRegisters.processorFlags[FlagAccessors::interruptDisable])
		{
			uint16_t pc = m_cpuRegisters.programCounter.to_ulong();
			push_stack(pc >> 8);
			push_stack(pc);
			push_stack(m_cpuRegisters.processorFlags.to_ulong());
			uint16_t interruptLocation = m_bus.read(0xFFFE);
			interruptLocation += m_bus.read(0xFFFF) << 8;
			m_cpuRegisters.programCounter = interruptLocation;
			interruptFlags::IRQ = false;
			m_cpuRegisters.processorFlags[FlagAccessors::interruptDisable] = true; // We ignore any further IRQs until the RTI, at which point the flag will be restored to its original value.
		}
		uint16_t pc = m_cpuRegisters.programCounter.to_ulong();
		uint8_t instruction = m_bus.read(pc++);
		m_cpuRegisters.programCounter = pc;
		currentInstruction = instructions[instruction];
		m_inactiveCycles = currentInstruction.cycles - 1;
#ifdef _DEBUG
		if (printDebug)
		{
			printDebugInfo();
		}
#endif
		fetchAddr(currentInstruction.mode);
		currentInstruction.func(this);
		if (cpuFlags::DMA)
		{
			oamDmaWrite(cpuFlags::DMAAddr);
			cpuFlags::DMA = false;
			cpuFlags::DMAAddr = 0;
		}
	}
	else
	{
		m_inactiveCycles--;
	}
	globalCounters::CPUCycleCount++;
}

void CPU::push_stack(uint8_t value)
{
	uint16_t addr = 0x0100;
	uint8_t stackPointer = m_cpuRegisters.stackPointer.to_ulong();
	addr += stackPointer;
	m_bus.write(addr, value);
	stackPointer -= 1;
	m_cpuRegisters.stackPointer = stackPointer;
}

uint8_t CPU::pop_stack()
{
	uint16_t addr = 0x0100;
	uint8_t stackPointer = m_cpuRegisters.stackPointer.to_ulong() + 1;
	addr += stackPointer;
	uint8_t value = m_bus.read(addr);
	m_cpuRegisters.stackPointer = stackPointer;
	return value;
}

void CPU::oamDmaWrite(uint8_t val)
{
	uint16_t addrPrefix = val << 8;
	for (int i = 0; i <= 0xFF; i++)
	{
		m_bus.write(0x2004, m_bus.read(addrPrefix + i));
	}
	m_inactiveCycles += OAMDMA_WRITE_CYCLES + ((globalCounters::CPUCycleCount + m_inactiveCycles) % 2); //The CPU should be suspended for 513/514 cycles after using OAMDMA.
}

#ifdef _DEBUG
void CPU::printDebugInfo()
{
	std::cout << "Program Counter: 0x" << HEX_FORMAT << m_cpuRegisters.programCounter.to_ulong() - 1;
	std::cout << " || Accumulator: 0x" << HEX_FORMAT << m_cpuRegisters.accumulator.to_ulong();
	std::cout << " || x Index: 0x" << HEX_FORMAT << m_cpuRegisters.xIndex.to_ulong();
	std::cout << " || y Index: 0x" << HEX_FORMAT << m_cpuRegisters.yIndex.to_ulong();
	std::cout << " || Flags: " << m_cpuRegisters.processorFlags.to_string();
	std::cout << " || " << currentInstruction.debugName << " with Addressing Mode " << currentInstruction.debugMode;
	if (currentInstruction.mode != AddressingModes::accumulator && currentInstruction.mode != AddressingModes::implied)
	{
		uint16_t addr = peekAddr(currentInstruction.mode);
		std::cout << ", Target Address is 0x" << HEX_FORMAT << addr;
	}
	else
	{
		std::cout << ", No Target Address";
	}
	std::cout << ", Value is " << HEX_FORMAT << static_cast<uint16_t>(peekValue()) << std::endl;
}

uint16_t CPU::peekAddr(AddressingModes mode) const
{
	uint16_t pc = m_cpuRegisters.programCounter.to_ulong();
	uint16_t tempAddr, tempArgAddr;
	uint8_t tempAddrZeroPage;
	switch (mode)
	{
	case AddressingModes::relative: // As it turns out, these modes do the same thing, they only have differences in hardware and not in effect.
	case AddressingModes::immediate:
		return pc++;
	case AddressingModes::zeroPage:
		return m_bus.peek(pc++);
	case AddressingModes::absolute:
		tempAddr = m_bus.peek(pc++);
		tempAddr += m_bus.peek(pc++) << 8;
		return tempAddr;
	case AddressingModes::implied:
	case AddressingModes::accumulator:
		break;
	case AddressingModes::indexedX:
		tempAddr = m_bus.peek(pc++);
		tempAddr += m_bus.peek(pc++) << 8;
		tempAddr += m_cpuRegisters.xIndex.to_ulong();
		return tempAddr;
	case AddressingModes::indexedY:
		tempAddr = m_bus.peek(pc++);
		tempAddr += m_bus.peek(pc++) << 8;
		tempAddr += m_cpuRegisters.yIndex.to_ulong();
		return tempAddr;
	case AddressingModes::zeroPageIndexedX:
		tempAddrZeroPage = m_bus.peek(pc++);
		tempAddrZeroPage += m_cpuRegisters.xIndex.to_ulong();
		return tempAddrZeroPage;
	case AddressingModes::zeroPageIndexedY:
		tempAddrZeroPage = m_bus.peek(pc++);
		tempAddrZeroPage += m_cpuRegisters.yIndex.to_ulong();
		return tempAddrZeroPage;
	case AddressingModes::indirect:
		tempArgAddr = m_bus.peek(pc++);
		tempArgAddr += m_bus.peek(pc++) << 8;
		tempAddr = m_bus.peek(tempArgAddr++);
		tempAddr += m_bus.peek(tempArgAddr) << 8;
		return tempAddr;
	case AddressingModes::preIndexedIndirect:
		tempAddrZeroPage = m_bus.peek(pc++) + m_cpuRegisters.xIndex.to_ulong();
		tempAddr = m_bus.peek(tempAddrZeroPage++);
		tempAddr += m_bus.peek(tempAddrZeroPage) << 8;
		return tempAddr;
	case AddressingModes::postIndexedIndirect:
		tempAddrZeroPage = m_bus.peek(pc++);
		tempAddr = m_bus.peek(tempAddrZeroPage++);
		tempAddr += m_bus.peek(tempAddrZeroPage) << 8;
		tempAddr += m_cpuRegisters.yIndex.to_ulong();
		return tempAddr;
	}
	return 0;
}

uint8_t CPU::peekValue() const
{
	if (currentInstruction.mode != AddressingModes::accumulator && currentInstruction.mode != AddressingModes::implied)
	{
		return m_bus.peek(peekAddr(currentInstruction.mode));
	}
	return m_cpuRegisters.accumulator.to_ulong();
}
#endif