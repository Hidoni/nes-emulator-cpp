matrix = [["BRK", "ORA (ind,X)", None, None, None, "ORA zpg", "ASL zpg", None, "PHP", "ORA #", "ASL A", None, None,"ORA abs", "ASL abs", None],
["BPL rel", "ORA (ind),Y", None, None, None,"ORA zpg,X", "ASL zpg,X", None, "CLC", "ORA abs,Y", None, None, None, "ORA abs,X", "ASL abs,X", None],
["JSR abs", "AND (ind,X)",None, None, "BIT zpg", "AND zpg", "ROL zpg", None, "PLP", "AND #", "ROL A", None, "BIT abs", "AND abs", "ROL abs", None],
["BMI rel", "AND (ind),Y", None, None, None,"AND zpg,X", "ROL zpg,X", None, "SEC", "AND abs,Y", None, None, None, "AND abs,X", "ROL abs,X", None],
["RTI", "EOR (ind,X)", None, None, None, "EOR zpg", "LSR zpg", None, "PHA", "EOR #", "LSR A", None, "JMP abs", "EOR abs", "LSR abs", None],
["BVC rel", "EOR (ind),Y", None, None, None, "EOR zpg,X", "LSR zpg,X", None, "CLI", "EOR abs,Y", None, None, None, "EOR abs,X", "LSR abs,X", None],
["RTS", "ADC (ind,X)", None, None, None, "ADC zpg", "ROR zpg", None, "PLA", "ADC #", "ROR A", None, "JMP (ind)", "ADC abs", "ROR abs", None],
["BVS rel", "ADC (ind),Y", None, None, None, "ADC zpg,X", "ROR zpg,X", None, "SEI", "ADC abs,Y", None, None, None, "ADC abs,X", "ROR abs,X", None],
[None, "STA (ind,X)", None, None, "STY zpg", "STA zpg", "STX zpg", None, "DEY", None, "TXA", None, "STY abs", "STA abs", "STX abs", None],
["BCC rel", "STA (ind),Y", None, None, "STY zpg,X", "STA zpg,X", "STX zpg,Y", None, "TYA", "STA abs,Y", "TXS", None, None, "STA abs,X", None, None],
["LDY #", "LDA (ind,X)", "LDX #", None, "LDY zpg", "LDA zpg", "LDX zpg", None, "TAY", "LDA #", "TAX", None, "LDY abs", "LDA abs", "LDX abs", None],
["BCS rel", "LDA (ind),Y", None, None, "LDY zpg,X", "LDA zpg,X", "LDX zpg,Y", None, "CLV", "LDA abs,Y", "TSX", None, "LDY abs,X", "LDA abs,X", "LDX abs,Y", None],
["CPY #", "CMP (ind,X)", None, None, "CPY zpg", "CMP zpg", "DEC zpg", None, "INY", "CMP #", "DEX", None, "CPY abs", "CMP abs", "DEC abs", None],
["BNE rel", "CMP (ind),Y", None, None, None, "CMP zpg,X", "DEC zpg,X", None, "CLD", "CMP abs,Y", None, None, None, "CMP abs,X", "DEC abs,X", None],
["CPX #", "SBC (ind,X)",None, None, "CPX zpg", "SBC zpg", "INC zpg", None, "INX", "SBC #", "NOP", None, "CPX abs", "SBC abs", "INC abs", None],
["BEQ rel", "SBC (ind),Y", None, None, None, "SBC zpg,X", "INC zpg,X", None, "SED", "SBC abs,Y", None, None, None, "SBC abs,X", "INC abs,X", None]]

cycles = [[7, 6, 2, 2, 2, 3, 5, 2, 3, 2, 2, 2, 2, 4, 6, 2],
[2, 5, 2, 2, 2, 4, 6, 2, 2, 4, 2, 2, 2, 4, 6, 2],
[6, 6, 2, 2, 3, 3, 5, 2, 4, 2, 2, 2, 4, 4, 6, 2],
[2, 5, 2, 2, 2, 4, 6, 2, 2, 4, 2, 2, 2, 4, 6, 2],
[6, 6, 2, 2, 2, 3, 5, 2, 3, 2, 2, 2, 3, 4, 6, 2],
[2, 5, 2, 2, 2, 4, 6, 2, 2, 4, 2, 2, 2, 4, 6, 2],
[6, 6, 2, 2, 2, 3, 5, 2, 4, 2, 2, 2, 6, 4, 6, 2],
[2, 5, 2, 2, 2, 4, 6, 2, 2, 4, 2, 2, 2, 4, 6, 2],
[2, 6, 2, 2, 3, 3, 3, 2, 2, 2, 2, 2, 4, 4, 4, 2],
[2, 6, 2, 2, 4, 4, 4, 2, 2, 5, 2, 2, 2, 4, 2, 2],
[2, 6, 2, 2, 3, 3, 3, 2, 2, 2, 2, 2, 4, 4, 4, 2],
[2, 5, 2, 2, 4, 4, 4, 2, 2, 4, 2, 2, 4, 4, 4, 2],
[2, 6, 2, 2, 3, 3, 5, 2, 2, 2, 2, 2, 4, 4, 6, 2],
[2, 5, 2, 2, 2, 4, 6, 2, 2, 4, 2, 2, 2, 4, 6, 2],
[2, 6, 2, 2, 3, 3, 5, 2, 2, 2, 2, 2, 4, 4, 6, 2],
[2, 5, 2, 2, 2, 4, 6, 2, 2, 4, 2, 2, 2, 4, 6, 2]]

isDebug = int(input("Debug Output?: "))
if isDebug == 0:
    isDebug = False
else:
    isDebug = True

for x in range(len(matrix)):
    row = matrix[x]
    for y in range(len(matrix[x])):
        opcode = matrix[x][y]
        if opcode is None:
            if isDebug:
                print("{&Instructions::ILL, AddressingModes::implied, 2, \"ILL\", \"Implied\"}", end=", ")
            else:
                print("{&Instructions::ILL, AddressingModes::implied, 2}", end=", ")
        else:
            mode = "ERROR"
            debugMode = ""
            type = opcode[4:]
            if len(opcode) == 3:
                mode = "implied"
                debugMode = "Implied"
            elif "A" == type:
                mode = "accumulator"
                debugMode = "Accumulator"
            elif "(ind,X)" == type:
                mode = "preIndexedIndirect"
                debugMode = "Pre-Indexed Indirect"
            elif "(ind),Y" == type:
                mode = "postIndexedIndirect"
                debugMode = "Post-Indexed Indirect"
            elif "#" == type:
                mode = "immediate"
                debugMode = "Immediate"
            elif "zpg" == type:
                mode = "zeroPage"
                debugMode = "Zero Page"
            elif "zpg,X" == type:
                mode = "zeroPageIndexedX"
                debugMode = "Zero Page Indexed with X"
            elif "zpg,Y" == type:
                mode = "zeroPageIndexedY"
                debugMode = "Zero Page Indexed with Y"
            elif "abs" == type:
                mode = "absolute"
                debugMode = "Absolute"
            elif "abs,X" == type:
                mode = "indexedX"
                debugMode = "Indexed with X"
            elif "abs,Y" == type:
                mode = "indexedY"
                debugMode = "Indexed with Y"
            elif "rel" == type:
                mode = "relative"
                debugMode = "Relative"
            elif "(ind)" == type:
                mode = "indirect"
                debugMode = "Indirect"
            if isDebug:
                print("{" + f"&Instructions::{opcode[:3]}, AddressingModes::{mode}, {cycles[x][y]}, \"{opcode[:3]}\", \"{debugMode}\"" + "}", end=", ")
            else:
                print("{" + f"&Instructions::{opcode[:3]}, AddressingModes::{mode}, {cycles[x][y]}" + "}", end=", ")
    print()
