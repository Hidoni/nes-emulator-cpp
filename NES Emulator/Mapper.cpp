#include "Mapper.h"
#include "MapperException.h"
#define INES_16k (size_t)0x4000
#define INES_8k (size_t)0x2000
#define INES_HEADER_PRG_SIZE 4
#define INES_HEADER_CHR_SIZE 5
#define INES_HEADER_PRGRAM_SIZE 8
#define MAPPER_PRG_BEGIN 0x6000
#define MAPPER_PRG_RAM_END 0x7FFF
#define MAPPER_PRG_ROM_END 0xFFFF

Mapper::Mapper(std::vector<uint8_t> rom) : m_rom(rom)
{
	auto prgBegin = std::vector<uint8_t>::const_iterator(m_rom.begin() + 16), prgEnd = prgBegin + (INES_16k * m_rom[INES_HEADER_PRG_SIZE]);
	m_prg = std::vector<uint8_t>(prgBegin, prgEnd);
	if (m_rom[INES_HEADER_CHR_SIZE])
	{
		auto chrBegin = prgEnd, chrEnd = chrBegin + (INES_8k * m_rom[INES_HEADER_CHR_SIZE]);
		m_chr = std::vector<uint8_t>(chrBegin, chrEnd);
		m_chrIsRAM = false;
	}
	else
	{
		m_chr.resize(INES_8k); // If the CHR Size header is 0, the board uses CHR-RAM, I'm setting this to 8k as a default fallback value even though I am pretty sure this mapper doesn't even implement CHR-RAM.
		m_chrIsRAM = true;
	}
	if (!m_rom[INES_HEADER_PRGRAM_SIZE])
	{
		m_prgRam.resize(INES_8k); // if prgram flag is 0, the default is 8k
	}
	else
	{
		m_prgRam.resize(INES_8k * m_rom[INES_HEADER_PRGRAM_SIZE]);
	}
}

uint8_t Mapper::readPRG(uint16_t addr)
{
	if (addr >= MAPPER_PRG_BEGIN && addr <= MAPPER_PRG_RAM_END)
	{
		return m_prgRam[addr % m_prgRam.size()];
	}
	else if (addr <= MAPPER_PRG_ROM_END)
	{
		return m_prg[addr % m_prg.size()];
	}
	throw MapperException("Value Not In Valid Memory Range For Mapper");
}

uint8_t Mapper::readCHR(uint16_t addr)
{
	if (addr <= m_chr.size())
	{
		return m_chr[addr];
	}
	throw MapperException("Value Not In Valid Memory Range For Mapper");
}

void Mapper::writePRG(uint16_t addr, uint8_t val)
{
	if (addr >= MAPPER_PRG_BEGIN && addr <= MAPPER_PRG_RAM_END)
	{
		m_prgRam[addr % m_prgRam.size()] = val;
		return;
	}
	throw MapperException("Tried To Write To PRG-ROM Area");
}

void Mapper::writeCHR(uint16_t addr, uint8_t val)
{
	if (m_chrIsRAM)
	{
		m_chr[addr % m_chr.size()] = val;
		return;
	}
	throw MapperException("Tried To Write To CHR-ROM Area");
}
