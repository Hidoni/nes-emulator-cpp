#pragma once
#include "Cartridge.h"
#include "objectAttributes.h"
#include <array>
#include <cstdlib>

class PPU;

class PPUMemoryBus
{
private:
	std::array<std::array<uint8_t, 0x0400>, 2>* m_nametables;
	std::array<std::array<std::array<uint8_t, 0xF>, 0xF>, 0x2>* m_patterntables;
	uint8_t bufferedData;
	Cartridge* m_cartridge;
	PPU* m_ppu;
public:
	std::array<uint8_t, 0x20>* m_palette;
	PPUMemoryBus(Cartridge* cartridge);
	uint8_t CPURead(uint16_t addr); // Used by the CPU to interface with the PPU
	void CPUWrite(uint16_t addr, uint8_t val);
	uint8_t videoRead(uint16_t addr);
	void videoWrite(uint16_t addr, uint8_t val);
	uint8_t OAMRead(uint8_t addr);
	void OAMWrite(uint8_t addr, uint8_t val);
	void DMAWrite(std::array<uint8_t, 0x100> data);
	void setPPU(PPU* ppu); // Since the memory bus is effectively an interface between the PPU and the CPU and also a read/write protector, It is a friend of the PPU and not the other way around.
#ifdef _DEBUG
	uint8_t CPUPeek(uint16_t addr) const;
	uint8_t videoPeek(uint16_t addr) const;
#endif
};