#pragma once
#include <exception>
#include <string>

class CartridgeException : std::exception
{
private:
	std::string error;
public:
	const char* what() const { return error.c_str(); };
	CartridgeException(std::string reason) : error(reason) {};
};