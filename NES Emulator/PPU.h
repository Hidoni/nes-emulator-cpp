#pragma once
#include "PPUMemoryBus.h"
#include "ShiftRegister.h"
#include "PPUAddrRegister.h"
#include "ObjectAttributes.h"
#include <bitset>
#include <SDL.h>

struct PPURegisters
{
	std::bitset<8> control; // PPUCTRL
	std::bitset<8> mask; // PPUMASK
	std::bitset<8> status; // PPUSTATUS
	std::bitset<8> objectAttributeMemoryAddress; // OAMADDR
	std::bitset<8> objectAttributeMemoryData; // OAMDATA
	PPUAddrRegister temporary, videoAddress;
	std::bitset<3> fineX;
};

class PPU
{
private:
	PPURegisters m_registers;
	PPUMemoryBus m_bus;
	bool oddFrame; // The PPU apparently takes one cycle longer every other frame
	bool writeToggle; // One write toggle is shared between both PPUSCROLL and PPUADDR.
	int m_scanline, m_scanlineCycle; // Scanline ranges from 0 to 261, cycles range from 0 to 340.
	ShiftRegister<uint16_t> patternTableHigh, patternTableLow;
	ShiftRegister<uint8_t> attributeTableHigh, attributeTableLow;
	uint8_t nametableLatch, patternHighLatch, patternLowLatch, attributeLatch;
	bool attributeLowVal, attributeHighVal;
	std::array<ObjectAttributes, 64> primaryOAM; // Holds all 64 sprites.
	std::array<ObjectAttributes, 8> secondaryOAM; // Holds 8 sprites to be rendered on this scanline.
	std::array<ShiftRegister<uint8_t>, 8> spritePatternHigh, spritePatternLow;
	std::array<std::bitset<8>, 8> spriteX, spriteAttributes;
	uint8_t secondaryOAMSize;
	bool spriteZeroNextScanline, spriteZeroInUse;
	SDL_Renderer* m_renderer;
	SDL_Surface* m_surface;
	

	// exec helper functions
	bool renderingEnabled();
	void shiftAll();
	void loadBackgroundShiftRegisters();
	void incrementHorizontalAddress();
	void incrementVerticalAddress();
	void copyHorizontalAddress();
	void copyVerticalAddress();
	void initializeSecondaryOAM(); 
	void actualSpriteEvaluation(); 
	void fetchSprites();
	// parts of exec
	void backgroundFetch();
	void evaluateSprites();
	void handlePixel();
	void drawPixel(uint8_t y, uint8_t x, uint8_t R, uint8_t G, uint8_t B);
	void render();
#ifdef _DEBUG
	void debugPatternTableRender();
	void debugPalettes();
#endif
public:
	PPU(PPUMemoryBus& bus, SDL_Renderer* renderer);
	void exec();
	friend class PPUMemoryBus;
};