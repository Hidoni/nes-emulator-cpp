#include "APU.h"
#include "interruptFlags.h"

// https://wiki.nesdev.com/w/index.php/APU_Frame_Counter
#define MODE_ZERO_SEQUENCER_STEP_ONE 7457
#define MODE_ZERO_SEQUENCER_STEP_TWO 14913
#define MODE_ZERO_SEQUENCER_STEP_THREE 22371
#define MODE_ZERO_SEQUENCER_STEP_FOUR 29828
#define MODE_ZERO_END 29830

#define MODE_ONE_SEQUENCER_STEP_ONE 7457
#define MODE_ONE_SEQUENCER_STEP_TWO 14913
#define MODE_ONE_SEQUENCER_STEP_THREE 22371
#define MODE_ONE_SEQUENCER_STEP_FOUR 29829
#define MODE_ONE_SEQUENCER_STEP_FIVE 37281
#define MODE_ONE_END 37282

APU::APU(APUMemoryBus bus, size_t sampleRate, SDL_AudioDeviceID audioDevice) : m_bus(bus), m_registers(), m_sampleRate(sampleRate), m_audioDevice(audioDevice)
{
	// https://wiki.nesdev.com/w/index.php/APU_Mixer
	for (int i = 0; i < m_pulseTable.size(); i++)
	{
		m_pulseTable[i] = 95.52 / (8128.0 / float(i) + 100);
	}
	for (int i = 0; i < m_otherChannelsTable.size(); i++)
	{
		m_otherChannelsTable[i] = 163.67 / (24329.0 / float(i) + 100);
	}
	filters[0] = new HighPassFilter(90, m_sampleRate);
	filters[1] = new HighPassFilter(440, m_sampleRate);
	filters[2] = new LowPassFilter(14000, m_sampleRate);
}

void APU::exec()
{
	m_registers.triangle.clockTimer();
	if (globalCounters::CPUCycleCount % 2 == 0)
	{
		m_registers.pulseOne.clockTimer();
		m_registers.pulseTwo.clockTimer();
		m_registers.noise.clockTimer();
	}
	if (!getFrameCounterMode())
	{
		switch (globalCounters::CPUCycleCount % MODE_ZERO_END)
		{
		case MODE_ZERO_SEQUENCER_STEP_ONE:
		case MODE_ZERO_SEQUENCER_STEP_THREE:
			m_registers.pulseOne.handleQuarterClock();
			m_registers.pulseTwo.handleQuarterClock();
			m_registers.triangle.handleQuarterClock();
			m_registers.noise.handleQuarterClock();
			break;
		case (MODE_ZERO_SEQUENCER_STEP_FOUR + 1):
			if (getFrameCounterInterrupt())
			{
				interruptFlags::IRQ = true;
			}
		case MODE_ZERO_SEQUENCER_STEP_TWO:
			m_registers.pulseOne.handleQuarterClock();
			m_registers.pulseTwo.handleQuarterClock();
			m_registers.triangle.handleQuarterClock();
			m_registers.noise.handleQuarterClock();
			m_registers.pulseOne.handleHalfClock();
			m_registers.pulseTwo.handleHalfClock();
			m_registers.triangle.handleHalfClock();
			m_registers.noise.handleHalfClock();
			break;
		case 0: // Counter "wraps around"
		case MODE_ZERO_SEQUENCER_STEP_FOUR:
			if (getFrameCounterInterrupt())
			{
				interruptFlags::IRQ = true;
			}
			break;
		}
	}
	else
	{
		switch (globalCounters::CPUCycleCount % MODE_ONE_END)
		{
		case MODE_ONE_SEQUENCER_STEP_ONE:
		case MODE_ONE_SEQUENCER_STEP_THREE:
			m_registers.pulseOne.handleQuarterClock();
			m_registers.pulseTwo.handleQuarterClock();
			m_registers.triangle.handleQuarterClock();
			m_registers.noise.handleQuarterClock();
			break;
		case MODE_ONE_SEQUENCER_STEP_TWO:
		case MODE_ONE_SEQUENCER_STEP_FIVE:
			m_registers.pulseOne.handleQuarterClock();
			m_registers.pulseTwo.handleQuarterClock();
			m_registers.triangle.handleQuarterClock();
			m_registers.noise.handleQuarterClock();
			m_registers.pulseOne.handleHalfClock();
			m_registers.pulseTwo.handleHalfClock();
			m_registers.triangle.handleHalfClock();
			m_registers.noise.handleHalfClock();
			break;
		case 0:
		case MODE_ONE_SEQUENCER_STEP_FOUR: // Apparently these do literally nothing.
			break;
		}
	}
	if ((globalCounters::CPUCycleCount % (1789773 / m_sampleRate)) == 0) // Output Sample.
	{
		float sample = outputSample();
		for (BaseFilter* filter : this->filters)
		{
			sample = filter->process(sample);
		}
		SDL_QueueAudio(m_audioDevice, &sample, sizeof(sample)); // Gonna try this with one sample at a time.
	}
}

float APU::outputSample()
{
#ifdef APU_USE_LINEAR_APPROXIMATION
	float pulse_output = 0.00752 * (m_registers.pulseOne.getValue() + m_registers.pulseTwo.getValue());
	int dmc = 0;
	float other_output = 0.00851 * m_registers.triangle.getValue() + 0.00494 * m_registers.noise.getValue() + 0.00335 * dmc;
#else
	float pulse_output = m_pulseTable[m_registers.pulseOne.getValue() + m_registers.pulseTwo.getValue()];
	float other_output = m_otherChannelsTable[3 * m_registers.triangle.getValue() + 2 * m_registers.noise.getValue() + 0]; // TODO: Replace 0 with DMC.
#endif
	return pulse_output + other_output;
}
