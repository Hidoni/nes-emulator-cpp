#pragma once
#include <cstdint>

// PPU Shift Registers, Felt like these belong in a separate file
// Specialized for 8 and 16 bit shifts
template <class T>
class ShiftRegister
{};

template<>
class ShiftRegister<uint16_t>
{
private:
	uint16_t value;
public:
	bool get(uint16_t index);
	void shift();
	void loadEight(uint8_t num);
};

template<>
class ShiftRegister<uint8_t>
{
private:
	uint8_t value;
public:
	bool get(uint8_t index);
	void shift();
	void shiftOr(bool val);
	void loadEight(uint8_t num);
	void flipValue();
};