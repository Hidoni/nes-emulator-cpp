#include "PPUMemoryBus.h"
#include "PPU.h"
#define CPU_TO_PPU_CONTROL 0x2000
#define CPU_TO_PPU_MASK 0x2001
#define CPU_TO_PPU_STATUS 0x2002
#define CPU_TO_PPU_OAMADDRESS 0x2003
#define CPU_TO_PPU_OAMDATA 0x2004
#define CPU_TO_PPU_SCROLL 0x2005
#define CPU_TO_PPU_ADDRESS 0x2006
#define CPU_TO_PPU_DATA 0x2007
#define CPU_TO_PPU_OAMDMA 0x4014
#define PPU_PATTERN_TABLE_END 0x1FFF
#define PPU_NAMETABLE_END 0x3EFF
#define PPU_NAMETABLE_MIRRORING_BEGIN 0x3000
#define NAMETABLE_ONE_END 0x23FF
#define NAMETABLE_TWO_END 0x27FF
#define NAMETABLE_THREE_END 0x2BFF
#define NAMETABLE_FOUR_END 0x2FFF


PPUMemoryBus::PPUMemoryBus(Cartridge* cartridge) : m_cartridge(cartridge), m_ppu(nullptr), bufferedData(0), m_nametables()
{
	m_nametables = new std::array<std::array<uint8_t, 0x0400>, 2>;
	m_patterntables = new std::array<std::array<std::array<uint8_t, 0xF>, 0xF>, 0x2>;
	m_palette = new std::array<uint8_t, 0x20>;
	m_nametables->data()[0].fill(0);
	m_nametables->data()[1].fill(0);
	m_palette->fill(0);
}

uint8_t PPUMemoryBus::CPURead(uint16_t addr)
{
	uint8_t returnVal = 0; 
	switch (addr)
	{
	case CPU_TO_PPU_STATUS:
		returnVal = m_ppu->m_registers.status.to_ulong();
		m_ppu->m_registers.status[7] = false;
		m_ppu->writeToggle = false;
		return returnVal;
		break;
	case CPU_TO_PPU_OAMDATA:
		return OAMRead(m_ppu->m_registers.objectAttributeMemoryAddress.to_ulong());
		break;
	/*
	The PPU is weird, it buffers the data you want to read and effectively only returns it on the next read
	which means that the first read also contains garbage data!
	However, if the data is coming from the palette, it isn't buffered and gets returned immediately, but it puts a wrong value on the buffer.
	*/
	case CPU_TO_PPU_DATA:
		if (m_ppu->m_registers.videoAddress.toNum() > PPU_NAMETABLE_END)
		{
			returnVal = videoRead(m_ppu->m_registers.videoAddress.toNum());
			bufferedData = videoRead(m_ppu->m_registers.videoAddress.toNum() - 0x1000);

		}
		else
		{
			returnVal = bufferedData;
			bufferedData = videoRead(m_ppu->m_registers.videoAddress.toNum());
		}
		m_ppu->m_registers.videoAddress.fromNum(m_ppu->m_registers.videoAddress.toNum() + (m_ppu->m_registers.control[2] ? 32 : 1));
		return returnVal;
	}
	return 0x0; // In case we had a write only register
}

void PPUMemoryBus::CPUWrite(uint16_t addr, uint8_t val)
{
	switch (addr)
	{
	case CPU_TO_PPU_CONTROL:
		m_ppu->m_registers.temporary.nametableSelect = val & 0x3; // At most, the 2 lowest bits will be on.
		m_ppu->m_registers.control = val;
		break;
	case CPU_TO_PPU_MASK:
		m_ppu->m_registers.mask = val;
		break;
	case CPU_TO_PPU_OAMADDRESS:
		m_ppu->m_registers.objectAttributeMemoryAddress = val;
		break;
	case CPU_TO_PPU_OAMDATA:
		OAMWrite(m_ppu->m_registers.objectAttributeMemoryAddress.to_ulong(), val);
		m_ppu->m_registers.objectAttributeMemoryAddress = m_ppu->m_registers.objectAttributeMemoryAddress.to_ulong() + 1;
		break;
	case CPU_TO_PPU_SCROLL:
		if (!m_ppu->writeToggle)
		{
			m_ppu->m_registers.temporary.coarseX = val >> 3;
			m_ppu->m_registers.fineX = val & 0x7;
		}
		else
		{
			m_ppu->m_registers.temporary.coarseY = val >> 3;
			m_ppu->m_registers.temporary.fineY = val & 0x7;
		}
		m_ppu->writeToggle = !m_ppu->writeToggle;
		break;
	case CPU_TO_PPU_ADDRESS:
		if (!m_ppu->writeToggle)
		{
			// Basing this on: https://wiki.nesdev.com/w/index.php/PPU_scrolling
			m_ppu->m_registers.temporary.coarseY[3] = val & 0x1;
			m_ppu->m_registers.temporary.coarseY[4] = val & 0x2;
			m_ppu->m_registers.temporary.nametableSelect[0] = val & 0x4;
			m_ppu->m_registers.temporary.nametableSelect[1] = val & 0x8;
			m_ppu->m_registers.temporary.fineY[0] = val & 0x10;
			m_ppu->m_registers.temporary.fineY[1] = val & 0x20;
			m_ppu->m_registers.temporary.fineY[2] = 0;
		}
		else
		{
			// Same here, though it's a little bit easier.
			m_ppu->m_registers.temporary.coarseX = val & 0x1f;
			m_ppu->m_registers.temporary.coarseY[0] = val & 0x20;
			m_ppu->m_registers.temporary.coarseY[1] = val & 0x40;
			m_ppu->m_registers.temporary.coarseY[2] = val & 0x80;
			m_ppu->m_registers.videoAddress = m_ppu->m_registers.temporary;
		}
		m_ppu->writeToggle = !m_ppu->writeToggle;
		break;
	case CPU_TO_PPU_DATA:
		videoWrite(m_ppu->m_registers.videoAddress.toNum(), val);
		m_ppu->m_registers.videoAddress.fromNum(m_ppu->m_registers.videoAddress.toNum() + (m_ppu->m_registers.control[2] ? 32 : 1));
		break;
	}
}

uint8_t PPUMemoryBus::videoRead(uint16_t addr)
{
	if (addr <= PPU_PATTERN_TABLE_END)
	{
		return m_cartridge->getMapper()->readCHR(addr);
	}
	else if (addr <= PPU_NAMETABLE_END)
	{
		if (addr >= PPU_NAMETABLE_MIRRORING_BEGIN)
		{
			addr -= 0x1000;
		}
		if (m_cartridge->mirroringType == mirroringTypes::horizontal)
		{
			if (addr <= NAMETABLE_TWO_END)
			{
				if (addr > NAMETABLE_ONE_END)
				{
					addr -= 0x400;
				}
				return m_nametables->data()[0].data()[addr - 0x2000]; // We need this to be in the range of 0 to 0x400.
			}
			else
			{
				if (addr > NAMETABLE_THREE_END)
				{
					addr -= 0x400;
				}
				return m_nametables->data()[1].data()[addr - 0x2800];
			}
		}
		else // vertical
		{
			if (addr <= NAMETABLE_ONE_END || (addr > NAMETABLE_TWO_END && addr <= NAMETABLE_THREE_END))
			{
				if (addr > NAMETABLE_ONE_END)
				{
					addr -= 0x800;
				}
				return m_nametables->data()[0].data()[addr - 0x2000];
			}
			else
			{
				if (addr > NAMETABLE_TWO_END)
				{
					addr -= 0x800;
				}
				return m_nametables->data()[1].data()[addr - 0x2400];
			}
		}
	}
	else // Palette.
	{
		addr &=  0x1F;
		if (addr == 0x10 || addr == 0x14 || addr == 0x18 || addr == 0x1C)
		{
			addr -= 0x10;
		}
		if (addr == 0x00 || addr == 0x04 || addr == 0x08 || addr == 0x0C)
		{
			addr = 0;
		}
		return m_palette->data()[addr];
	}
}

void PPUMemoryBus::videoWrite(uint16_t addr, uint8_t val)
{
	if (addr <= PPU_PATTERN_TABLE_END)
	{
		m_cartridge->getMapper()->writeCHR(addr, val);
	}
	else if (addr <= PPU_NAMETABLE_END)
	{
		if (addr >= PPU_NAMETABLE_MIRRORING_BEGIN)
		{
			addr -= 0x1000;
		}
		if (m_cartridge->mirroringType == mirroringTypes::horizontal)
		{
			if (addr <= NAMETABLE_TWO_END)
			{
				if (addr > NAMETABLE_ONE_END)
				{
					addr -= 0x400;
				}
				m_nametables->data()[0].data()[addr - 0x2000] = val; // We need this to be in the range of 0 to 0x400.
			}
			else
			{
				if (addr > NAMETABLE_THREE_END)
				{
					addr -= 0x400;
				}
				m_nametables->data()[1].data()[addr - 0x2800] = val;
			}
		}
		else // vertical
		{
			if (addr <= NAMETABLE_ONE_END || (addr > NAMETABLE_TWO_END&& addr <= NAMETABLE_THREE_END))
			{
				if (addr > NAMETABLE_ONE_END)
				{
					addr -= 0x800;
				}
				m_nametables->data()[0].data()[addr - 0x2000] = val;
			}
			else
			{
				if (addr > NAMETABLE_TWO_END)
				{
					addr -= 0x800;
				}
				m_nametables->data()[1].data()[addr - 0x2400] = val;
			}
		}
	}
	else // Palette.
	{
		addr &= 0x1F;
		if (addr == 0x10 || addr == 0x14 || addr == 0x18 || addr == 0x1C)
		{
			addr -= 0x10;
		}
		m_palette->data()[addr] = val;
	}
}

uint8_t PPUMemoryBus::OAMRead(uint8_t addr)
{
	switch (addr % 4)
	{
	case 0:
		return m_ppu->primaryOAM[addr / 4].yPosition.to_ulong();
	case 1:
		return m_ppu->primaryOAM[addr / 4].tileIndex.to_ulong();
	case 2:
		return m_ppu->primaryOAM[addr / 4].objectAttributes.to_ulong();
	case 3:
		return m_ppu->primaryOAM[addr / 4].xPosition.to_ulong();
	}
}

void PPUMemoryBus::OAMWrite(uint8_t addr, uint8_t val)
{
	switch (addr % 4)
	{
	case 0:
		m_ppu->primaryOAM[addr / 4].yPosition = val;
		break;
	case 1:
		m_ppu->primaryOAM[addr / 4].tileIndex = val;
		break;
	case 2:
		m_ppu->primaryOAM[addr / 4].objectAttributes = val;
		break;
	case 3:
		m_ppu->primaryOAM[addr / 4].xPosition = val;
		break;
	}
}

void PPUMemoryBus::DMAWrite(std::array<uint8_t, 0x100> data)
{
	for (int i = 0; i < 64; i++)
	{
		uint8_t index = m_ppu->m_registers.objectAttributeMemoryAddress.to_ulong();
		int oamIndex = index * 4;
		m_ppu->primaryOAM[index] = ObjectAttributes(data[oamIndex++], data[oamIndex++], data[oamIndex++], data[oamIndex++]);
		for (int j = 0; j < 4; j++)
		{
			index++;
			if (index == 64)
			{
				index = 0;
			}
		}
		m_ppu->m_registers.objectAttributeMemoryAddress = index;
	}
}

void PPUMemoryBus::setPPU(PPU* ppu)
{
	m_ppu = ppu;
}

#ifdef _DEBUG
uint8_t PPUMemoryBus::CPUPeek(uint16_t addr) const
{
	switch (addr)
	{
	case CPU_TO_PPU_STATUS:
		return m_ppu->m_registers.status.to_ulong();
	case CPU_TO_PPU_OAMDATA:
		return 0;
	case CPU_TO_PPU_DATA:
		if (m_ppu->m_registers.videoAddress.toNum() > PPU_NAMETABLE_END)
		{
			return videoPeek(m_ppu->m_registers.videoAddress.toNum());
		}
		return bufferedData;
	}
	return 0x0;
}

uint8_t PPUMemoryBus::videoPeek(uint16_t addr) const
{
	if (addr <= PPU_PATTERN_TABLE_END)
	{
		return m_cartridge->getMapper()->readCHR(addr);
	}
	else if (addr <= PPU_NAMETABLE_END)
	{
		if (addr >= PPU_NAMETABLE_MIRRORING_BEGIN)
		{
			addr -= 0x1000;
		}
		if (m_cartridge->mirroringType == mirroringTypes::horizontal)
		{
			if (addr <= NAMETABLE_TWO_END)
			{
				if (addr > NAMETABLE_ONE_END)
				{
					addr -= 0x400;
				}
				return m_nametables->data()[0].data()[addr - 0x2000]; // We need this to be in the range of 0 to 0x400.
			}
			else
			{
				if (addr > NAMETABLE_THREE_END)
				{
					addr -= 0x400;
				}
				return m_nametables->data()[1].data()[addr - 0x2800];
			}
		}
		else // vertical
		{
			if (addr <= NAMETABLE_ONE_END || (addr > NAMETABLE_TWO_END&& addr <= NAMETABLE_THREE_END))
			{
				if (addr > NAMETABLE_ONE_END)
				{
					addr -= 0x800;
				}
				return m_nametables->data()[0].data()[addr - 0x2000];
			}
			else
			{
				if (addr > NAMETABLE_TWO_END)
				{
					addr -= 0x800;
				}
				return m_nametables->data()[1].data()[addr - 0x2400];
			}
		}
	}
	else // Palette.
	{
		addr &= 0x1F;
		if (addr == 0x10 || addr == 0x14 || addr == 0x18 || addr == 0x1C)
		{
			addr -= 0x10;
		}
		return m_palette->data()[addr];
	}
}
#endif