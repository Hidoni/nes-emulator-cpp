#pragma once
#include <exception>
#include <string>

class MapperException : std::exception
{
private:
	std::string error;
public:
	const char* what() const { return error.c_str(); };
	MapperException(std::string reason) : error(reason) {};
};