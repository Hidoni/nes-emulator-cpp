#include "PPUAddrRegister.h"

PPUAddrRegister PPUAddrRegister::operator=(const PPUAddrRegister& other)
{
	this->coarseX = other.coarseX;
	this->coarseY = other.coarseY;
	this->nametableSelect = other.nametableSelect;
	this->fineY = other.fineY;
	return *this;
}

uint16_t PPUAddrRegister::toNum()
{
	uint16_t returnVal = fineY.to_ulong() << 12;
	returnVal += nametableSelect.to_ulong() << 10;
	returnVal += coarseY.to_ulong() << 5;
	returnVal += coarseX.to_ulong();
	return returnVal;
}

void PPUAddrRegister::fromNum(uint16_t num)
{
	fineY = num >> 12;
	nametableSelect = (num & 0xC00) >> 10;
	coarseY = (num & 0x3E0) >> 5;
	coarseX = (num & 0x1F);
}

uint16_t PPUAddrRegister::toNametableIndex()
{
	return toNum() & 0x0FFF;
}

uint16_t PPUAddrRegister::divideCoarse()
{
	uint16_t returnVal = (coarseY.to_ulong() >> 2) << 3;
	returnVal += coarseX.to_ulong() >> 2;
	return returnVal;
}
