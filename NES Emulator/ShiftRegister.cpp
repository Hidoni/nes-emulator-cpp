#include "ShiftRegister.h"
#define HIGHER_HALF_ON 0xFF00
#define SIXTEEN_BIT_SHIFT 15
#define EIGHT_BIT_SHIFT 7
#define SHIFT_TO_TOP_EIGHT 8

bool ShiftRegister<uint16_t>::get(uint16_t index)
{
	bool returnVal = value & index;
	return returnVal;
}

void ShiftRegister<uint16_t>::shift()
{
	value <<= 1;
}

void ShiftRegister<uint16_t>::loadEight(uint8_t num)
{
	value = (value & HIGHER_HALF_ON) | num; // The value will only keep its low 8 bits on, while num only has the 8 top bits on
}

bool ShiftRegister<uint8_t>::get(uint8_t index)
{
	bool returnVal = value & index;
	return returnVal;
}

void ShiftRegister<uint8_t>::shift()
{
	value <<= 1;
}

void ShiftRegister<uint8_t>::shiftOr(bool val)
{
	value = (value << 1) | val;
}


void ShiftRegister<uint8_t>::loadEight(uint8_t num)
{
	value = num; // The eight bit shifter just loads itself immediately.
}

void ShiftRegister<uint8_t>::flipValue()
{
	static uint8_t lookup[16] = {
	0x0, 0x8, 0x4, 0xc, 0x2, 0xa, 0x6, 0xe,
	0x1, 0x9, 0x5, 0xd, 0x3, 0xb, 0x7, 0xf, };

	value = (lookup[value & 0b1111] << 4) | lookup[value >> 4];
}


