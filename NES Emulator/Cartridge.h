#pragma once
#include <cstdint>
#include <string>
#include "Mapper.h"

enum class mirroringTypes {horizontal, vertical}; // There's more so we have room for expansion with an enum over like, a bool.

class Cartridge
{
private:
	Mapper* m_mapper; // For future purposes, hold a pointer to the mapper so that we can inherit from mapper while maintaining the same base class
	bool loadedMapper = false;
	inline bool validateFile(std::vector<uint8_t> data);
public:
	Cartridge(std::string romPath);
	Cartridge() : m_mapper(nullptr), mirroringType(mirroringTypes::horizontal) {}; // Empty Constructor to be able to have a Cartridge instance in main.
	~Cartridge();
	Mapper* getMapper();
	mirroringTypes mirroringType;
};