#pragma once
#include <bitset>

struct PPUAddrRegister
{
	std::bitset<5> coarseX;
	std::bitset<5> coarseY;
	std::bitset<2> nametableSelect;
	std::bitset<3> fineY;
	PPUAddrRegister operator=(const PPUAddrRegister& other);
	/* Kinda dirty hacks to quickly turn the 4 seperate bitsets into their respective numbers. */
	uint16_t toNum();
	void fromNum(uint16_t num);
	uint16_t toNametableIndex(); // Just save a bit of time.
	uint16_t divideCoarse();
};