#include "Instructions.h"
#include "CPU.h"
#include "NotImplemented.h"
#define MOST_SIGNIFICANT_BIT 7
#define MAX_POSITIVE 127

// To avoid reusing code in all of the branch instructions, this inline helper function exists.
inline void Instructions::branch(CPU* cpu)
{
	cpu->fetchValue();
	uint8_t val = cpu->targetValue;
	uint16_t pc = cpu->m_cpuRegisters.programCounter.to_ulong();
	cpu->m_inactiveCycles += 1;
	if (val > MAX_POSITIVE) // The value is signed...
	{
		val = 0xFF - val;
		if ((pc - val) >> 4 != pc >> 4)
		{
			cpu->m_inactiveCycles += 1;
		}
		pc -= val + 1;
	}
	else
	{
		if ((pc + val) >> 4 != pc >> 4)
		{
			cpu->m_inactiveCycles += 1;
		}
		pc += val;
	}
	cpu->m_cpuRegisters.programCounter = pc;
}

void Instructions::ADC(CPU* cpu)
{
	cpu->fetchValue();
	// 16 bit to keep track of overflow.
	uint16_t acc = cpu->m_cpuRegisters.accumulator.to_ulong() + cpu->targetValue + cpu->m_cpuRegisters.processorFlags[FlagAccessors::carry]; // It's add with carry
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::overflow] = (~((uint16_t)cpu->m_cpuRegisters.accumulator.to_ulong() ^ (uint16_t)cpu->targetValue) & ((uint16_t)cpu->m_cpuRegisters.accumulator.to_ulong() ^ acc)) & 0x80;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = acc & 0x80;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !(acc & 0xFF);
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::carry] = acc > 0xFF;
	cpu->m_cpuRegisters.accumulator = acc;
}

void Instructions::AND(CPU* cpu)
{
	cpu->fetchValue();
	uint8_t acc = cpu->m_cpuRegisters.accumulator.to_ulong();
	acc &= cpu->targetValue;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = acc & 0x80;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !acc;
	cpu->m_cpuRegisters.accumulator = acc;
}

void Instructions::ASL(CPU* cpu)
{
	cpu->fetchValue();
	uint16_t val = (uint16_t)cpu->targetValue << 1;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::carry] = cpu->targetValue >> 7;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = val & 0x80;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !(val & 0xFF);
	if (cpu->currentInstruction.mode == AddressingModes::accumulator)
	{
		cpu->m_cpuRegisters.accumulator = val;
	}
	else
	{
		
		cpu->m_bus.write(cpu->targetAddr, val);
	}
}

void Instructions::BCC(CPU* cpu)
{
	if (!cpu->m_cpuRegisters.processorFlags[FlagAccessors::carry])
	{
		branch(cpu);
	}
}

void Instructions::BCS(CPU* cpu)
{
	if (cpu->m_cpuRegisters.processorFlags[FlagAccessors::carry])
	{
		branch(cpu);
	}
}

void Instructions::BEQ(CPU* cpu)
{
	if (cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero])
	{
		branch(cpu);
	}
}

void Instructions::BIT(CPU* cpu)
{
	cpu->fetchValue();
	uint8_t newVal = cpu->m_cpuRegisters.accumulator.to_ulong() & cpu->targetValue;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !(newVal);
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::overflow] = cpu->targetValue & 0x40;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = cpu->targetValue & 0x80;
}

void Instructions::BMI(CPU* cpu)
{
	if (cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative])
	{
		branch(cpu);
	}
}

void Instructions::BNE(CPU* cpu)
{
	if (!cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero])
	{
		branch(cpu);
	}
}

void Instructions::BPL(CPU* cpu)
{
	if (!cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative])
	{
		branch(cpu);
	}
}

void Instructions::BRK(CPU* cpu)
{
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::breakFlag] = 1;
	uint16_t pc = cpu->m_cpuRegisters.programCounter.to_ulong();
	cpu->push_stack(pc >> 8);
	cpu->push_stack(pc);
	cpu->push_stack(cpu->m_cpuRegisters.processorFlags.to_ulong());
	uint16_t interruptLocation = cpu->m_bus.read(0xFFFE);
	interruptLocation += cpu->m_bus.read(0xFFFF) << 8;
	cpu->m_cpuRegisters.programCounter = interruptLocation;
}

void Instructions::BVC(CPU* cpu)
{
	if (!cpu->m_cpuRegisters.processorFlags[FlagAccessors::overflow])
	{
		branch(cpu);
	}
}

void Instructions::BVS(CPU* cpu)
{
	if (cpu->m_cpuRegisters.processorFlags[FlagAccessors::overflow])
	{
		branch(cpu);
	}
}

void Instructions::CLC(CPU* cpu)
{
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::carry] = 0;
}

void Instructions::CLD(CPU* cpu)
{
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::decimal] = 0; // Decimal should always be off in the NES.
}

void Instructions::CLI(CPU* cpu)
{
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::interruptDisable] = 0;
}

void Instructions::CLV(CPU* cpu)
{
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::overflow] = 0;
}

void Instructions::CMP(CPU* cpu)
{
	cpu->fetchValue();
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::carry] = cpu->m_cpuRegisters.accumulator.to_ulong() >= cpu->targetValue;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = cpu->m_cpuRegisters.accumulator.to_ulong() == cpu->targetValue;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = (cpu->m_cpuRegisters.accumulator.to_ulong() - cpu->targetValue) >> 7;
}

void Instructions::CPX(CPU* cpu)
{
	cpu->fetchValue();
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::carry] = cpu->m_cpuRegisters.xIndex.to_ulong() >= cpu->targetValue;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = cpu->m_cpuRegisters.xIndex.to_ulong() == cpu->targetValue;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = (cpu->m_cpuRegisters.xIndex.to_ulong() - cpu->targetValue) >> 7;
}

void Instructions::CPY(CPU* cpu)
{
	cpu->fetchValue();
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::carry] = cpu->m_cpuRegisters.yIndex.to_ulong() >= cpu->targetValue;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = cpu->m_cpuRegisters.yIndex.to_ulong() == cpu->targetValue;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = (cpu->m_cpuRegisters.yIndex.to_ulong() - cpu->targetValue) >> 7;
}

void Instructions::DEC(CPU* cpu)
{
	cpu->fetchValue();
	uint8_t newVal = cpu->targetValue - 1;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !newVal;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = newVal >> 7;
	cpu->m_bus.write(cpu->targetAddr, newVal);
}

void Instructions::DEX(CPU* cpu)
{
	cpu->m_cpuRegisters.xIndex = cpu->m_cpuRegisters.xIndex.to_ulong() - 1;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !(cpu->m_cpuRegisters.xIndex.to_ulong());
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = cpu->m_cpuRegisters.xIndex[MOST_SIGNIFICANT_BIT];
}

void Instructions::DEY(CPU* cpu)
{
	cpu->m_cpuRegisters.yIndex = cpu->m_cpuRegisters.yIndex.to_ulong() - 1;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !(cpu->m_cpuRegisters.yIndex.to_ulong());
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = cpu->m_cpuRegisters.yIndex[MOST_SIGNIFICANT_BIT];
}

void Instructions::EOR(CPU* cpu)
{
	cpu->fetchValue();
	cpu->m_cpuRegisters.accumulator = cpu->m_cpuRegisters.accumulator.to_ulong() ^ cpu->targetValue;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !(cpu->m_cpuRegisters.accumulator.to_ulong());
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = cpu->m_cpuRegisters.accumulator[MOST_SIGNIFICANT_BIT];
}

void Instructions::INC(CPU* cpu)
{
	cpu->fetchValue();
	uint8_t newVal = cpu->targetValue + 1;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !newVal;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = newVal >> 7;
	cpu->m_bus.write(cpu->targetAddr, newVal);
}

void Instructions::INX(CPU* cpu)
{
	cpu->m_cpuRegisters.xIndex = cpu->m_cpuRegisters.xIndex.to_ulong() + 1;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !(cpu->m_cpuRegisters.xIndex.to_ulong());
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = cpu->m_cpuRegisters.xIndex[MOST_SIGNIFICANT_BIT];
}

void Instructions::INY(CPU* cpu)
{
	cpu->m_cpuRegisters.yIndex = cpu->m_cpuRegisters.yIndex.to_ulong() + 1;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !(cpu->m_cpuRegisters.yIndex.to_ulong());
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = cpu->m_cpuRegisters.yIndex[MOST_SIGNIFICANT_BIT];
}

void Instructions::JMP(CPU* cpu)
{
	cpu->m_cpuRegisters.programCounter = cpu->targetAddr;
}

void Instructions::JSR(CPU* cpu)
{
	uint16_t pc = cpu->m_cpuRegisters.programCounter.to_ulong() - 1;
	cpu->push_stack(pc >> 8);
	cpu->push_stack(pc);
	cpu->m_cpuRegisters.programCounter = cpu->targetAddr;
}

void Instructions::LDA(CPU* cpu)
{
	cpu->fetchValue();
	cpu->m_cpuRegisters.accumulator = cpu->targetValue;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !(cpu->m_cpuRegisters.accumulator.to_ulong());
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = cpu->m_cpuRegisters.accumulator[MOST_SIGNIFICANT_BIT];
}

void Instructions::LDX(CPU* cpu)
{
	cpu->fetchValue();
	cpu->m_cpuRegisters.xIndex = cpu->targetValue;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !(cpu->m_cpuRegisters.xIndex.to_ulong());
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = cpu->m_cpuRegisters.xIndex[MOST_SIGNIFICANT_BIT];
}

void Instructions::LDY(CPU* cpu)
{
	cpu->fetchValue();
	cpu->m_cpuRegisters.yIndex = cpu->targetValue;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !(cpu->m_cpuRegisters.yIndex.to_ulong());
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = cpu->m_cpuRegisters.yIndex[MOST_SIGNIFICANT_BIT];
}

void Instructions::LSR(CPU* cpu)
{
	if (cpu->currentInstruction.mode == AddressingModes::accumulator)
	{
		uint8_t acc = cpu->m_cpuRegisters.accumulator.to_ulong() >> 1;
		cpu->m_cpuRegisters.processorFlags[FlagAccessors::carry] = cpu->m_cpuRegisters.accumulator[0];
		cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = acc & 0x80;
		cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !acc;
		cpu->m_cpuRegisters.accumulator = acc;
	}
	else
	{
		cpu->fetchValue();
		uint8_t val = cpu->targetValue >> 1;
		cpu->m_cpuRegisters.processorFlags[FlagAccessors::carry] = cpu->targetValue & 0x1;
		cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = val & 0x80;
		cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !val;
		cpu->m_bus.write(cpu->targetAddr, val);
	}
}

void Instructions::NOP(CPU* cpu)
{
	//The spanish inquisition.
}

void Instructions::ORA(CPU* cpu)
{
	cpu->fetchValue();
	uint8_t acc = cpu->m_cpuRegisters.accumulator.to_ulong() | cpu->targetValue;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = acc > MAX_POSITIVE;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !acc;
	cpu->m_cpuRegisters.accumulator = acc;
}

void Instructions::PHA(CPU* cpu)
{
	cpu->push_stack(cpu->m_cpuRegisters.accumulator.to_ulong());
}

void Instructions::PHP(CPU* cpu)
{
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::breakFlag] = 1;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::alwaysOne] = 1;
	cpu->push_stack(cpu->m_cpuRegisters.processorFlags.to_ulong());
}

void Instructions::PLA(CPU* cpu)
{
	uint8_t acc = cpu->pop_stack();
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = acc > MAX_POSITIVE;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !acc;
	cpu->m_cpuRegisters.accumulator = acc;
}

void Instructions::PLP(CPU* cpu)
{
	cpu->m_cpuRegisters.processorFlags = cpu->pop_stack();
}

void Instructions::ROL(CPU* cpu)
{
	if (cpu->currentInstruction.mode == AddressingModes::accumulator)
	{
		std::bitset<8> newTarget = cpu->m_cpuRegisters.accumulator;
		bool carry = cpu->m_cpuRegisters.processorFlags[FlagAccessors::carry];
		cpu->m_cpuRegisters.processorFlags[FlagAccessors::carry] = newTarget[MOST_SIGNIFICANT_BIT];
		newTarget = newTarget.to_ulong() << 1;
		newTarget[0] = carry;

		cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !newTarget.to_ulong();
		cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = newTarget.to_ulong() > MAX_POSITIVE;

		cpu->m_cpuRegisters.accumulator = newTarget;
	}
	else
	{
		cpu->fetchValue();
		std::bitset<8> newTarget = cpu->targetValue;
		bool carry = cpu->m_cpuRegisters.processorFlags[FlagAccessors::carry];
		cpu->m_cpuRegisters.processorFlags[FlagAccessors::carry] = newTarget[MOST_SIGNIFICANT_BIT];
		newTarget = newTarget.to_ulong() << 1;
		newTarget[0] = carry;

		cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !newTarget.to_ulong();
		cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = newTarget.to_ulong() > MAX_POSITIVE;

		cpu->m_bus.write(cpu->targetAddr, newTarget.to_ulong());
	}
}

void Instructions::ROR(CPU* cpu)
{
	if (cpu->currentInstruction.mode == AddressingModes::accumulator)
	{
		std::bitset<8> newTarget = cpu->m_cpuRegisters.accumulator;
		bool carry = cpu->m_cpuRegisters.processorFlags[FlagAccessors::carry];
		cpu->m_cpuRegisters.processorFlags[FlagAccessors::carry] = newTarget[0];
		newTarget = newTarget.to_ulong() >> 1;
		newTarget[MOST_SIGNIFICANT_BIT] = carry;
		cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !newTarget.to_ulong();
		cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = newTarget.to_ulong() > MAX_POSITIVE;
		cpu->m_cpuRegisters.accumulator = newTarget;
	}
	else
	{
		cpu->fetchValue();
		std::bitset<8> newTarget = cpu->targetValue;
		bool carry = cpu->m_cpuRegisters.processorFlags[FlagAccessors::carry];
		cpu->m_cpuRegisters.processorFlags[FlagAccessors::carry] = newTarget[0];
		newTarget = newTarget.to_ulong() >> 1;
		newTarget[MOST_SIGNIFICANT_BIT] = carry;
		cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !newTarget.to_ulong();
		cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = newTarget.to_ulong() > MAX_POSITIVE;
		cpu->m_bus.write(cpu->targetAddr, newTarget.to_ulong());
	}
}

void Instructions::RTI(CPU* cpu)
{
	cpu->m_cpuRegisters.processorFlags = cpu->pop_stack();
	uint16_t pc = cpu->pop_stack();
	pc += cpu->pop_stack() << 8;
	cpu->m_cpuRegisters.programCounter = pc;
}

void Instructions::RTS(CPU* cpu)
{
	uint16_t pc = cpu->pop_stack();
	pc += cpu->pop_stack() << 8;
	pc += 1;
	cpu->m_cpuRegisters.programCounter = pc;
}

void Instructions::SBC(CPU* cpu)
{
	cpu->fetchValue();
	cpu->targetValue = cpu->targetValue ^ 0xFF;
	uint16_t acc = cpu->m_cpuRegisters.accumulator.to_ulong() + cpu->targetValue + cpu->m_cpuRegisters.processorFlags[FlagAccessors::carry]; // It's add with carry
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::overflow] = (~((uint16_t)cpu->m_cpuRegisters.accumulator.to_ulong() ^ (uint16_t)cpu->targetValue) & ((uint16_t)cpu->m_cpuRegisters.accumulator.to_ulong() ^ acc)) & 0x80;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = acc & 0x80;
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !(acc & 0xFF);
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::carry] = acc > 0xFF;
	cpu->m_cpuRegisters.accumulator = acc;
}

void Instructions::SEC(CPU* cpu)
{
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::carry] = true;
}

void Instructions::SED(CPU* cpu)
{
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::decimal] = true;
}

void Instructions::SEI(CPU* cpu)
{
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::interruptDisable] = true;
}

void Instructions::STA(CPU* cpu)
{
	cpu->m_bus.write(cpu->targetAddr, cpu->m_cpuRegisters.accumulator.to_ulong());
}

void Instructions::STX(CPU* cpu)
{
	cpu->m_bus.write(cpu->targetAddr, cpu->m_cpuRegisters.xIndex.to_ulong());
}

void Instructions::STY(CPU* cpu)
{
	cpu->m_bus.write(cpu->targetAddr, cpu->m_cpuRegisters.yIndex.to_ulong());
}

void Instructions::TAX(CPU* cpu)
{
	cpu->m_cpuRegisters.xIndex = cpu->m_cpuRegisters.accumulator.to_ulong();
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !(cpu->m_cpuRegisters.xIndex.to_ulong());
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = cpu->m_cpuRegisters.xIndex[7];
}

void Instructions::TAY(CPU* cpu)
{
	cpu->m_cpuRegisters.yIndex = cpu->m_cpuRegisters.accumulator.to_ulong();
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !(cpu->m_cpuRegisters.yIndex.to_ulong());
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = cpu->m_cpuRegisters.yIndex[7];
}

void Instructions::TSX(CPU* cpu)
{
	cpu->m_cpuRegisters.xIndex = cpu->m_cpuRegisters.stackPointer.to_ulong();
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !(cpu->m_cpuRegisters.xIndex.to_ulong());
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = cpu->m_cpuRegisters.xIndex[7];
}

void Instructions::TXA(CPU* cpu)
{
	cpu->m_cpuRegisters.accumulator = cpu->m_cpuRegisters.xIndex.to_ulong();
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !(cpu->m_cpuRegisters.accumulator.to_ulong());
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = cpu->m_cpuRegisters.accumulator[7];
}

void Instructions::TXS(CPU* cpu)
{
	cpu->m_cpuRegisters.stackPointer = cpu->m_cpuRegisters.xIndex.to_ulong();
}

void Instructions::TYA(CPU* cpu)
{
	cpu->m_cpuRegisters.accumulator = cpu->m_cpuRegisters.yIndex.to_ulong();
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::zero] = !(cpu->m_cpuRegisters.accumulator.to_ulong());
	cpu->m_cpuRegisters.processorFlags[FlagAccessors::negative] = cpu->m_cpuRegisters.accumulator[7];
}

void Instructions::ILL(CPU* cpu)
{
	throw NotImplemented();
}