#include "InputCollector.h"
#include <SDL_keyboard.h>
#include <iostream>
#define A_BUTTON 0
#define B_BUTTON 1
#define SELECT 2
#define START 3
#define UP 4
#define DOWN 5
#define LEFT 6
#define RIGHT 7

InputCollector::InputCollector(const uint8_t* KeyboardState) : bits(0), index(0), update(false)
{
	SDL_KeyArr = KeyboardState;
}

void InputCollector::setUpdate()
{
	update = !update;
	if (!update)
	{
		bits[A_BUTTON] = SDL_KeyArr[SDL_Scancode::SDL_SCANCODE_Z];
		bits[B_BUTTON] = SDL_KeyArr[SDL_Scancode::SDL_SCANCODE_X];
		bits[SELECT] = SDL_KeyArr[SDL_Scancode::SDL_SCANCODE_S];
		bits[START] = SDL_KeyArr[SDL_Scancode::SDL_SCANCODE_D];
		bits[UP] = SDL_KeyArr[SDL_Scancode::SDL_SCANCODE_UP];
		bits[DOWN] = SDL_KeyArr[SDL_Scancode::SDL_SCANCODE_DOWN];
		bits[LEFT] = SDL_KeyArr[SDL_Scancode::SDL_SCANCODE_LEFT];
		bits[RIGHT] = SDL_KeyArr[SDL_Scancode::SDL_SCANCODE_RIGHT];
		index = 0;
	}
}

bool InputCollector::getBit()
{
	if (update)
	{
		return SDL_KeyArr[SDL_Scancode::SDL_SCANCODE_Z];
	}
	if (index <= RIGHT)
	{
		return bits[index++];
	}
	return true; // Reads after the first 8 bits always return 1.
}
