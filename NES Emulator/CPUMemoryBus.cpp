#include "CPUMemoryBus.h"
#include "NotImplemented.h"
#include "interruptFlags.h"
#define CPU_INTERNAL_MEMORY_SIZE 0x0800
#define CPU_INTERNAL_RAM_END 0x1FFF
#define CPU_PPU_REGISTERS_END 0x3FFF
#define CPU_OAMDMA 0x4014
#define CPU_JOYSTICK_ONE 0x4016
#define CPU_JOYSTICK_TWO 0x4017
#define CPU_CARTRIDGE_SPACE 0x4020
#define CPU_APU_REGISTERS_END 0x4017
#define CPU_APU_STATUS 0x4015

CPUMemoryBus::CPUMemoryBus(Cartridge* cartridge, APUMemoryBus& apu, PPUMemoryBus& ppu, InputCollector& one, InputCollector& two) : m_cartridge(cartridge), m_internal(CPU_INTERNAL_MEMORY_SIZE), m_controllerOne(one), m_controllerTwo(two), m_ppuBus(ppu), m_apuBus(apu)
{}

uint8_t CPUMemoryBus::read(uint16_t addr)
{
	if (addr <= CPU_INTERNAL_RAM_END)
	{
		return m_internal[addr % CPU_INTERNAL_MEMORY_SIZE];
	}
	else if (addr <= CPU_PPU_REGISTERS_END)
	{
		return m_ppuBus.CPURead(addr);
	}
	else if (addr == CPU_OAMDMA)
	{
		return m_ppuBus.CPURead(addr);
	}
	else if (addr == CPU_JOYSTICK_ONE)
	{
		return m_controllerOne.getBit();
	}
	else if (addr == CPU_JOYSTICK_TWO)
	{
		return m_controllerTwo.getBit();
	}
	else if (addr == CPU_APU_STATUS)
	{
		return m_apuBus.CPURead(addr);
	}
	else if (addr < CPU_CARTRIDGE_SPACE)
	{
		throw NotImplemented();
	}
	else
	{
		return m_cartridge->getMapper()->readPRG(addr);
	}
}

void CPUMemoryBus::write(uint16_t addr, uint8_t val)
{
	if (addr <= CPU_INTERNAL_RAM_END)
	{
		m_internal[addr % CPU_INTERNAL_MEMORY_SIZE] = val;
	}
	else if (addr <= CPU_PPU_REGISTERS_END)
	{
		m_ppuBus.CPUWrite(addr, val);
	}
	else if (addr == CPU_OAMDMA)
	{
		cpuFlags::DMA = true;
		cpuFlags::DMAAddr = val;
	}
	else if (addr == CPU_JOYSTICK_ONE)
	{
		m_controllerOne.setUpdate();
		m_controllerTwo.setUpdate();
	}
	else if (addr <= CPU_APU_REGISTERS_END)
	{
		m_apuBus.CPUWrite(addr, val);
	}
	else if (addr < CPU_CARTRIDGE_SPACE)
	{
		// throw NotImplemented();
	}
	else
	{
		m_cartridge->getMapper()->writePRG(addr, val);
	}
}

void CPUMemoryBus::DMAWrite(std::array<uint8_t, 0x100> data)
{
	m_ppuBus.DMAWrite(data);
}

#ifdef _DEBUG
uint8_t CPUMemoryBus::peek(uint16_t addr) const
{
	if (addr <= CPU_INTERNAL_RAM_END)
	{
		return m_internal[addr % CPU_INTERNAL_MEMORY_SIZE];
	}
	else if (addr <= CPU_PPU_REGISTERS_END)
	{
		return m_ppuBus.CPUPeek(addr);
	}
	else if (addr == CPU_OAMDMA)
	{
		return m_ppuBus.CPUPeek(addr);
	}
	else if (addr == CPU_JOYSTICK_ONE || addr == CPU_JOYSTICK_TWO)
	{
		return 0; // As if no button is pressed.
	}
	else if (addr < CPU_CARTRIDGE_SPACE)
	{
		return 0; // throw NotImplemented();
	}
	else
	{
		return m_cartridge->getMapper()->readPRG(addr);
	}
}
#endif
