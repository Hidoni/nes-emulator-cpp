#pragma once
#include "APUMemoryBus.h"
#include "APUChannels.h"
#include "APUNoiseFilters.h"
#include "SDL.h"
#include <iostream>
#include <bitset>
#include <array>

// https://wiki.nesdev.com/w/index.php/APU_Mixer#Emulation
struct APURegisters
{
	PulseChannel pulseOne, pulseTwo;
	TriangleChannel triangle;
	NoiseChannel noise;
	DMCChannel dmc;

	std::bitset<8> controlAndStatus;
	std::bitset<8> frameCounter;
	
	APURegisters() : pulseOne(true), pulseTwo(false), triangle(), noise(), dmc() 
	{};
};

class APU
{
private:
	std::array<float, 31> m_pulseTable;
	std::array<float, 203> m_otherChannelsTable;
	BaseFilter* filters[3];
	size_t m_sampleRate;
	SDL_AudioDeviceID m_audioDevice;
	APUMemoryBus m_bus;
	APURegisters m_registers;
	bool getFrameCounterMode() { return m_registers.frameCounter[GET_BIT_SEVEN]; }
	bool getFrameCounterInterrupt() { return m_registers.frameCounter[GET_BIT_SIX]; }
	void clearFrameCounterInterrupt() { m_registers.frameCounter[GET_BIT_SIX] = false; }
public:
	APU(APUMemoryBus bus, size_t sampleRate, SDL_AudioDeviceID audioDevice);
	void exec();
	float outputSample();
	friend class APUMemoryBus;
};