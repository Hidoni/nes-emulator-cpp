#pragma once
#include <bitset>
#include "CPUMemoryBus.h"
#include "Instructions.h"

struct CPURegisters
{
	std::bitset<8> accumulator; // A
	std::bitset<8> xIndex, yIndex; // X, Y
	std::bitset<16> programCounter; // pc
	std::bitset<8> stackPointer; // S
	std::bitset<8> processorFlags; // P
};

enum FlagAccessors
{
	carry, zero, interruptDisable, decimal, breakFlag, alwaysOne, overflow, negative
};

class CPU
{
private:
	CPURegisters m_cpuRegisters;
	CPUMemoryBus& m_bus;
	int m_inactiveCycles;
	uint8_t targetValue, targetAddrZeroPage;
	uint16_t targetAddr;
	Instruction currentInstruction;
	uint8_t pop_stack();
	void push_stack(uint8_t value);
	void fetchAddr(AddressingModes mode);
	void fetchValue();
#ifdef _DEBUG
public:
	bool printDebug = true;
private:
	void printDebugInfo();
	uint16_t peekAddr(AddressingModes mode) const;
	uint8_t peekValue() const;
#endif
public:
	CPU(CPUMemoryBus& bus);
	void exec_instruction();
	friend class Instructions;
	void oamDmaWrite(uint8_t val);
};