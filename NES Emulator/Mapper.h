#pragma once
#include <cstdint>
#include <vector>

class Mapper
{
protected:
	std::vector<uint8_t> m_rom, m_prg, m_prgRam, m_chr;
	bool m_chrIsRAM;
public:
	Mapper(std::vector<uint8_t> rom);
	// As it turns out, the PRG areas of ROM are accessed by the CPU, and the CHR areas of ROM are accessed by the PPU, and thus need to be handled seperately.
	uint8_t readPRG(uint16_t addr);
	uint8_t readCHR(uint16_t addr);
	void writePRG(uint16_t addr, uint8_t val);
	void writeCHR(uint16_t addr, uint8_t val);
};