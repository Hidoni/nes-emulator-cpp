#include "APUChannels.h"
#define APU_PULSE_ONE_ATTRIBUTES 0x4000
#define APU_PULSE_ONE_SWEEP 0x4001
#define APU_PULSE_ONE_TIMER_LOW 0x4002
#define APU_PULSE_ONE_TIMER_HIGH 0x4003
#define APU_PULSE_TWO_ATTRIBUTES 0x4004
#define APU_PULSE_TWO_SWEEP 0x4005
#define APU_PULSE_TWO_TIMER_LOW 0x4006
#define APU_PULSE_TWO_TIMER_HIGH 0x4007
#define APU_TRIANGLE_ATTRIBUTES 0x4008
#define APU_TRIANGLE_TIMER_LOW 0x400A
#define APU_TRIANGLE_TIMER_HIGH 0x400B
#define APU_NOISE_ATTRIBUTES 0x400C
#define APU_NOISE_TYPE 0x400E
#define APU_NOISE_LENGTH_COUNTER_LOAD 0x400F
#define APU_DMC_ATTRIBUTES 0x4010
#define APU_DMC_DIRECT_LOAD 0x4011
#define APU_DMC_SAMPLE_ADDRESS 0x4012
#define APU_DMC_SAMPLE_LENGTH 0x4013

LengthCounter& APUChannel::getLengthCounter()
{
	return m_lengthCounter;
}


// Pulse Channel Functions
PulseChannel::PulseChannel(bool isFirst) : m_sweepUnit(isFirst)
{}

void PulseChannel::handleQuarterClock()
{
	m_envelope.clock();
}

void PulseChannel::handleHalfClock()
{
	m_lengthCounter.clock();
	m_sweepUnit.clock(m_timer);
}

void PulseChannel::clockTimer()
{
	if (m_timer.clock())
	{
		m_generator.clock();
	}
}

void PulseChannel::handleCPUWrite(uint16_t addr, uint8_t value)
{
	switch (addr)
	{
	case APU_PULSE_ONE_ATTRIBUTES:
	case APU_PULSE_TWO_ATTRIBUTES:
		m_generator.setDuty(value >> 6);
		m_lengthCounter.setHalt(value & 0x20);
		m_envelope.setCounterLoopEnabled(value & 0x20);
		m_envelope.setConstantVolumeEnabled(value & 0x10);
		m_envelope.setConstantVolume(value & 0xF);
		break;
	case APU_PULSE_ONE_SWEEP:
	case APU_PULSE_TWO_SWEEP:
		m_sweepUnit.setEnabled(value & 0x80);
		m_sweepUnit.setDividerPeriod((value & 0x70) >> 4, m_timer);
		m_sweepUnit.setNegate(value & 0x8);
		m_sweepUnit.setShiftAmount(value & 0x7);
		m_sweepUnit.reload();
		break;
	case APU_PULSE_ONE_TIMER_LOW:
	case APU_PULSE_TWO_TIMER_LOW:
		m_timer.setLowBitsOfDividerPeriod(value);
		break;
	case APU_PULSE_ONE_TIMER_HIGH:
	case APU_PULSE_TWO_TIMER_HIGH:
		m_timer.setHighBitsOfDividerPeriod(value & 0x7);
		m_lengthCounter.setCounter(value >> 3);
		m_envelope.restartEnvelope();
		m_generator.restart();
		break;
	}
}

uint8_t PulseChannel::getValue()
{
	if (m_sweepUnit.channelDisabled() || m_lengthCounter.disableChannel())
	{
		return 0;
	}
	return m_envelope.getVolume() * m_generator.getValue();
}

// Triangle Channel Functions
TriangleChannel::TriangleChannel()
{
	m_timer.setMinimumPeriod(2);
};

void TriangleChannel::handleQuarterClock()
{
	m_linearCounter.clock();
}

void TriangleChannel::handleHalfClock()
{
	m_lengthCounter.clock();
}

void TriangleChannel::clockTimer()
{
	if (m_timer.clock())
	{
		if (m_linearCounter.getDividerCounter() && m_lengthCounter.getCounter())
		{
			m_generator.clock();
		}
	}
}

void TriangleChannel::handleCPUWrite(uint16_t addr, uint8_t value)
{
	switch(addr)
	{
	case APU_TRIANGLE_ATTRIBUTES:
		m_lengthCounter.setHalt(value & 0x80);
		m_linearCounter.handleWrite(value);
		break;
	case APU_TRIANGLE_TIMER_LOW:
		m_timer.setLowBitsOfDividerPeriod(value);
		break;
	case APU_TRIANGLE_TIMER_HIGH:
		m_timer.setHighBitsOfDividerPeriod(value & 0x7);
		m_linearCounter.reload();
		m_lengthCounter.setCounter(value >> 3);
		break;
	}
}

uint8_t TriangleChannel::getValue()
{
	return m_generator.getValue();
}

// Noise Channel
void NoiseChannel::setTimerPeriod(uint8_t lookupIndex)
{
	const static std::vector<uint16_t> ntscPeriods = { 4, 8, 16, 32, 64, 96, 128, 160, 202, 254, 380, 508, 762, 1016, 2034, 4068 };
	m_timer.setDividerPeriod((ntscPeriods[lookupIndex] / 2) - 1);
}

NoiseChannel::NoiseChannel()
{
	m_envelope.setCounterLoopEnabled(true);
}

void NoiseChannel::handleQuarterClock()
{
	m_envelope.clock();
}

void NoiseChannel::handleHalfClock()
{
	m_lengthCounter.clock();
}

void NoiseChannel::clockTimer()
{
	if (m_timer.clock())
	{
		m_register.clock();
	}
}

void NoiseChannel::handleCPUWrite(uint16_t addr, uint8_t value)
{
	switch(addr)
	{
	case APU_NOISE_ATTRIBUTES:
		m_lengthCounter.setHalt(value & 0x20); // Bit 5
		m_envelope.setConstantVolumeEnabled(value & 0x10); // Bit 4
		m_envelope.setConstantVolume(value & 0xF); // 4 low bits.
		break;
	case APU_NOISE_TYPE:
		m_register.setMode(value & 0x80); // Bit 7
		setTimerPeriod(value & 0xF); // 4 low bits.
		break;
	case APU_NOISE_LENGTH_COUNTER_LOAD:
		m_lengthCounter.setCounter(value >> 3);
		m_envelope.restartEnvelope();
	}
}

uint8_t NoiseChannel::getValue()
{
	if (m_register.disableChannel() || m_lengthCounter.disableChannel())
	{
		return 0;
	}
	return m_envelope.getVolume();
}
