#pragma once
#include <cstdint>

// IRQ Vector: 0xFFFE-0xFFFF, NMI Vector: 0xFFFA-0xFFFB, NMI Ignores the Ignore Interrupts flag.
static struct interruptFlags
{
	static bool IRQ;
	static bool NMI;
};

static struct cpuFlags
{
	static bool DMA;
	static uint8_t DMAAddr;
};

static struct globalCounters
{
	static unsigned long long CPUCycleCount;
};