#pragma once
// Code from: https://github.com/daniel5151/ANESE/blob/master/src/nes/apu/filters.h
// Seems to remove background noise artifacts.


typedef unsigned int hertz;

class BaseFilter {
protected:
    const double RC, dt;
public:
    virtual double process(double sample) = 0;

    virtual ~BaseFilter() = default;
    BaseFilter(hertz f, hertz sample_rate)
        : RC{ 1.0 / (2 * 3.1415928535 * double(f)) }
        , dt{ 1.0 / double(sample_rate) }
    {}
};

// https://en.wikipedia.org/wiki/Low-pass_filter
class LowPassFilter final : public BaseFilter {
private:
    const double a;
    struct { double x, y; } prev;
public:
    virtual ~LowPassFilter() = default;
    LowPassFilter(hertz f, hertz sample_rate)
        : BaseFilter(f, sample_rate)
        , a{ this->dt / (this->RC + this->dt) }
        , prev{ 0, 0 }
    {}

    double process(double x) override {
        double y = this->a * x + (1 - this->a) * this->prev.y;
        this->prev = { x, y };
        return y;
    }
};

// https://en.wikipedia.org/wiki/High-pass_filter
class HighPassFilter final : public BaseFilter {
private:
    const double a;
    struct { double x, y; } prev;
public:
    virtual ~HighPassFilter() = default;
    HighPassFilter(hertz f, hertz sample_rate)
        : BaseFilter(f, sample_rate)
        , a{ this->RC / (this->RC + this->dt) }
        , prev{ 0, 0 }
    {}

    double process(double x) override {
        double y = this->a * this->prev.y + this->a * (x - this->prev.x);
        this->prev = { x, y };
        return y;
    }
};