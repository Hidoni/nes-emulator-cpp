#pragma once
#include "Cartridge.h"
#include <iostream>

class APU;

class APUMemoryBus
{
private:
	Cartridge* m_cart;
	APU* m_apu;

public:
	APUMemoryBus(Cartridge* cart);
	void setAPU(APU* apu);
	uint8_t CPURead(uint16_t addr);
	void CPUWrite(uint16_t addr, uint8_t val);
};