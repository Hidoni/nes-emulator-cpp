#pragma once
#include <bitset>

class InputCollector
{
private:
	std::bitset<8> bits, pressed; // The order is as follows: A, B, Select, Start, Up, Down, Left, Right.
	uint8_t index;
	bool update;
	const uint8_t* SDL_KeyArr;
public:
	InputCollector(const uint8_t* KeyboardState);
	void setUpdate();
	bool getBit();
};