#include "APUMemoryBus.h"
#include "APU.h"
#define APU_PULSE_ONE_BEGIN 0x4000
#define APU_PULSE_ONE_END 0x4003
#define APU_PULSE_TWO_BEGIN 0x4004
#define APU_PULSE_TWO_END 0x4007
#define APU_TRIANGLE_BEGIN 0x4008
#define APU_TRIANGLE_END 0x400B
#define APU_NOISE_BEGIN 0x400C
#define APU_NOISE_END 0x400F
#define APU_DMC_BEGIN 0x4010
#define APU_DMC_END 0x4013
#define APU_CONTROL 0x4015
#define APU_STATUS 0x4015
#define APU_FRAME_COUNTER 0x4017

APUMemoryBus::APUMemoryBus(Cartridge* cart) : m_cart(cart)
{
}

void APUMemoryBus::setAPU(APU* apu)
{
	m_apu = apu;
}

uint8_t APUMemoryBus::CPURead(uint16_t addr)
{
	switch (addr)
	{
	case APU_STATUS:
		m_apu->clearFrameCounterInterrupt();
		m_apu->m_registers.controlAndStatus[0] = m_apu->m_registers.pulseOne.getLengthCounter().getCounter();
		m_apu->m_registers.controlAndStatus[1] = m_apu->m_registers.pulseTwo.getLengthCounter().getCounter();
		m_apu->m_registers.controlAndStatus[2] = m_apu->m_registers.triangle.getLengthCounter().getCounter();
		m_apu->m_registers.controlAndStatus[3] = m_apu->m_registers.noise.getLengthCounter().getCounter();
		return m_apu->m_registers.controlAndStatus.to_ulong();
	default:
		return 0;
	}
}

void APUMemoryBus::CPUWrite(uint16_t addr, uint8_t val)
{
	if (addr >= APU_PULSE_ONE_BEGIN && addr <= APU_PULSE_ONE_END)
	{
		m_apu->m_registers.pulseOne.handleCPUWrite(addr, val);
	}
	else if (addr >= APU_PULSE_TWO_BEGIN && addr <= APU_PULSE_TWO_END)
	{
		m_apu->m_registers.pulseTwo.handleCPUWrite(addr, val);
	}
	else if (addr >= APU_TRIANGLE_BEGIN && addr <= APU_TRIANGLE_END)
	{
		m_apu->m_registers.triangle.handleCPUWrite(addr, val);
	}
	else if (addr >= APU_NOISE_BEGIN && addr <= APU_NOISE_END)
	{
		m_apu->m_registers.noise.handleCPUWrite(addr, val);
	}
	else if (addr >= APU_DMC_BEGIN && addr <= APU_DMC_END)
	{

	}
	else if (addr == APU_CONTROL)
	{
		m_apu->m_registers.pulseOne.getLengthCounter().setEnabled(val & 0x1);
		m_apu->m_registers.pulseTwo.getLengthCounter().setEnabled(val & 0x2);
		m_apu->m_registers.triangle.getLengthCounter().setEnabled(val & 0x4);
		m_apu->m_registers.noise.getLengthCounter().setEnabled(val & 0x8);
	}
	else if (addr == APU_FRAME_COUNTER)
	{
		m_apu->m_registers.frameCounter = val;
	}
}
