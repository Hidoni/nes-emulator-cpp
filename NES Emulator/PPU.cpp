#include "PPU.h"
#include "interruptFlags.h"
#include "NESColor.h"
#define PPU_SCANLINE_VISIBLE_END 239
#define PPU_VERTICAL_SCROLL_RELOAD_START 280
#define PPU_VERTICAL_SCROLL_RELOAD_END 304
#define PPU_SCANLINE_CYCLES 340
#define PPU_PRE_RENDER_SCANLINE 261
#define PPU_BACKGROUND_FETCH_BEGIN 1
#define PPU_BACKGROUND_FETCH_END 256
#define PPU_PIXEL_DRAW_BEGIN 1
#define PPU_PIXEL_DRAW_END 256
#define PPU_NEXT_FETCH_BEGIN 321
#define PPU_NEXT_FETCH_END 336
#define PPU_SCANLINE_VBLANK_LINE 241
#define PPU_NAMETABLE_BEGIN 0x2000
#define PPU_ATTRIBUTE_BEGIN 0x23C0
#define PPU_PAST_BACKGROUND_FETCH 257
#define PPU_INITIAL_SCANLINE_DURATION 240
#define PPU_COPY_HORIZONTAL_BEGIN 280
#define PPU_COPY_HORIZONTAL_END 304
#define PPU_SPRITE_ZERO_HIT_FLAG 6
#define TEXTURE_WIDTH 256
#define TEXTURE_HEIGHT 240
#define OAM_SPRITE_CAPACITY 64
#define SECONDARY_OAM_SPRITE_CAPACITY 8
#define PPU_SPRITE_EVALUATION_BEGIN 0
#define PPU_SPRITE_EVALUATION_END 340
#define PPU_SECONDARY_OAM_INITIALIZE_BEGIN 1
#define PPU_SECONDARY_OAM_INITIALIZE_END 64
#define PPU_ACTUAL_EVALUATION_BEGIN 65
#define PPU_ACTUAL_EVALUATION_END 192
#define PPU_SPRITE_FETCH_BEGIN 257
#define PPU_SPRITE_FETCH_END 320
#define PPU_SPRITE_OVERFLOW 5
#define PPU_SPRITE_SIZE 5
#define PPU_SPRITE_LARGE 16
#define PPU_SPRITE_SMALL 8
#define OAM_FLIP_Y 7
#define OAM_FLIP_X 6


PPU::PPU(PPUMemoryBus& bus, SDL_Renderer* renderer) : m_bus(bus), m_renderer(renderer), oddFrame(false), writeToggle(false), m_scanline(PPU_INITIAL_SCANLINE_DURATION), m_scanlineCycle(0), patternTableHigh(), patternTableLow(), attributeTableHigh(), attributeTableLow(), secondaryOAMSize(0)
{
	m_surface = SDL_CreateRGBSurfaceWithFormat(0, TEXTURE_WIDTH, TEXTURE_HEIGHT, 0, SDL_PIXELFORMAT_RGBA8888); // We can then use rendercopy to scale it up.	
}

bool PPU::renderingEnabled()
{
	return m_registers.mask[3] || m_registers.mask[4];
}

void PPU::shiftAll()
{
	patternTableLow.shift();
	patternTableHigh.shift();
	attributeTableLow.shiftOr(attributeLowVal); // Required for palette precision.
	attributeTableHigh.shiftOr(attributeHighVal);

	if (m_registers.mask[4] && m_scanlineCycle >= 1 && m_scanlineCycle <= 257)
	{
		for (unsigned int i = 0; i < 8; i++)
		{
			if (spriteX[i].to_ulong() > 0)
			{ // Subtract one until it's zero, then we know it's time to render.
				spriteX[i] = spriteX[i].to_ulong() - 1;
			}
			else
			{
				spritePatternLow[i].shift();
				spritePatternHigh[i].shift();
			}
		}
	}
}

void PPU::loadBackgroundShiftRegisters()
{
	patternTableLow.loadEight(patternLowLatch);
	patternTableHigh.loadEight(patternHighLatch);
	attributeLowVal = attributeLatch & 0x1;
	attributeHighVal = attributeLatch & 0x2;
}

void PPU::incrementHorizontalAddress()
{
	if (renderingEnabled()) // Increments don't happen when both are off, as the programmer may be trying to write to video themselves.
	{
		
		if (m_registers.videoAddress.coarseX.to_ulong() == 31)
		{
			m_registers.videoAddress.coarseX = 0;
			m_registers.videoAddress.nametableSelect[0] = !m_registers.videoAddress.nametableSelect[0];
		}
		else
		{
			m_registers.videoAddress.coarseX = m_registers.videoAddress.coarseX.to_ulong() + 1;
		}
	}
}

void PPU::incrementVerticalAddress()
{
	if (renderingEnabled())
	{
		if (m_registers.videoAddress.fineY.to_ulong() < 7)
		{
			m_registers.videoAddress.fineY = m_registers.videoAddress.fineY.to_ulong() + 1;
		}
		else
		{
			m_registers.videoAddress.fineY = 0;
			uint8_t coarseY = m_registers.videoAddress.coarseY.to_ulong();
			if (coarseY == 29)
			{
				m_registers.videoAddress.coarseY = 0;
				m_registers.videoAddress.nametableSelect[1] = !m_registers.videoAddress.nametableSelect[1];
			}
			else if (coarseY == 31)
			{
				m_registers.videoAddress.coarseY = 0;
			}
			else
			{
				m_registers.videoAddress.coarseY = coarseY + 1;
			}
		}
	}
}

void PPU::copyHorizontalAddress()
{
	if (renderingEnabled())
	{
		m_registers.videoAddress.coarseX = m_registers.temporary.coarseX;
		m_registers.videoAddress.nametableSelect[0] = m_registers.temporary.nametableSelect[0];
	}
}

void PPU::copyVerticalAddress()
{
	if (renderingEnabled())
	{
		m_registers.videoAddress.coarseY = m_registers.temporary.coarseY;
		m_registers.videoAddress.fineY = m_registers.temporary.fineY;
		m_registers.videoAddress.nametableSelect[1] = m_registers.temporary.nametableSelect[1];
	}
}

void PPU::render()
{
#ifdef _DEBUG
	// debugPatternTableRender();
#endif
	SDL_Texture* texture = SDL_CreateTextureFromSurface(m_renderer, m_surface);
	SDL_RenderCopy(m_renderer, texture, nullptr, nullptr);
#ifdef _DEBUG
	// debugPalettes();
#endif
	SDL_RenderPresent(m_renderer);
	SDL_DestroyTexture(texture);
}

#ifdef _DEBUG
void PPU::debugPatternTableRender()
{
	// Name Table Drawing Code
	for (uint8_t y = 0; y < 16; y++)
	{
		for (uint8_t x = 0; x < 16; x++)
		{
			uint16_t offset = y * 256 + x * 16;
			for (uint16_t row = 0; row < 8; row++)
			{
				uint8_t tileLow = m_bus.videoRead(offset + row);
				uint8_t tileHigh = m_bus.videoRead(offset + row + 8);
				for (uint16_t col = 0; col < 8; col++)
				{
					uint8_t pixel = (tileLow & 0x01) + (tileHigh & 0x01);
					tileLow >>= 1;
					tileHigh >>= 1;
					NESColor color = NESColors[m_bus.videoRead(0x3F00 + pixel)];
					drawPixel(y * 8 + row, x * 8 + (7 - col), color.R, color.G, color.B);
				}
			}
		}
	}
}

void PPU::debugPalettes()
{
	// Palette Debug Code
	for (int i = 0; i < 8; i++)
	{
		SDL_Rect rect = { 0, 640 + i * 40, 40, 40 };
		for (int x = 0; x < 4; x++)
		{
			NESColor color = NESColors[m_bus.videoRead(0x3F00 + i * 4 + x)];
			SDL_SetRenderDrawColor(m_renderer, color.R, color.G, color.B, 255);
			SDL_RenderFillRect(m_renderer, &rect);
			rect.x += 40;
		}
	}
}
#endif

void PPU::drawPixel(uint8_t y, uint8_t x, uint8_t R, uint8_t G, uint8_t B)
{
	if (SDL_LockSurface(m_surface))
	{
		throw std::exception(SDL_GetError());
	}

	uint32_t color = SDL_MapRGB(m_surface->format, R, G, B);

	// Before setting the color, we need to know where we have to place it.
	uint32_t* pixel = (uint32_t*)((uint8_t*)m_surface->pixels + y * m_surface->pitch + x * sizeof *pixel); // Now we can set the pixel(s) we want.
	*pixel = color;
	SDL_UnlockSurface(m_surface);
}

void PPU::handlePixel()
{
	uint8_t bgPalette = 0, bgIndex = 0;
	uint8_t sprIndex = 0, sprPalette = 0;
	bool sprPriority = false;
	NESColor color = NESColors[0x3F];
	if (m_registers.mask[3])
	{
		uint16_t shiftIndex = 0x8000 >> m_registers.fineX.to_ulong();
		uint8_t shiftPalette = 0x80 >> m_registers.fineX.to_ulong();
		bgIndex = patternTableHigh.get(shiftIndex) << 1 | patternTableLow.get(shiftIndex);
		bgPalette = attributeTableHigh.get(shiftPalette) << 1| attributeTableLow.get(shiftPalette);
		uint16_t addr = 0x3F00;
		if (bgIndex)
		{
			addr += bgPalette << 2 | bgIndex;
		}
		color = NESColors[m_bus.videoRead(addr) & (m_registers.mask[0] ? 0x30 : 0x3F)];
	}
	if (m_registers.mask[4])
	{
		for (unsigned int i = 0; i < 8; i++)
		{
			if (!spriteX[i].to_ulong())
			{
				sprIndex = spritePatternHigh[i].get(0x80) << 1 | spritePatternLow[i].get(0x80);
				sprPalette = spriteAttributes[i][1] << 1 | spriteAttributes[i][0];
				sprPriority = !spriteAttributes[i][5];
				if (i == 0 && sprIndex && bgPalette && spriteZeroInUse && renderingEnabled() && (m_scanlineCycle - 1) != 255) // Non Transparent
				{
					m_registers.status[PPU_SPRITE_ZERO_HIT_FLAG] = true;
				}
				if (sprIndex && (sprPriority || !bgIndex))
				{
					color = NESColors[m_bus.videoRead(0x3F10 | (sprPalette << 2) | sprIndex) & 0x3F];
					break;
				}
			}
		}
	}
	drawPixel(m_scanline, m_scanlineCycle - 1, color.R, color.G, color.B);
}

void PPU::backgroundFetch()
{
	uint16_t startAddr;
	shiftAll();
	switch ((m_scanlineCycle - 1) % 8) // Since zero is idle, by decreasing the counter by one we'll have a number in the range of 0-255 that will show what part o
	{
	case 0: // Each of these takes two PPU cycles, hence why we only have even cases.
		loadBackgroundShiftRegisters();
		nametableLatch = m_bus.videoRead(PPU_NAMETABLE_BEGIN | m_registers.videoAddress.toNametableIndex());
		break;
	case 2:
		attributeLatch = m_bus.videoRead(PPU_ATTRIBUTE_BEGIN | (m_registers.videoAddress.nametableSelect.to_ulong() << 10) | ((m_registers.videoAddress.coarseY.to_ulong() / 4) << 3) | (m_registers.videoAddress.coarseX.to_ulong() / 4));
		if (m_registers.videoAddress.coarseY.to_ulong() & 0x02) // We are in the top half
		{
			attributeLatch >>= 4;
		}
		if (m_registers.videoAddress.coarseX.to_ulong() & 0x02) // We are in the left half
		{
			attributeLatch >>= 2;
		}
		attributeLatch &= 0x3; // Depending on how far we shifted, we might have more than two bits on, and we need specifically the two least significant bits on.
		break;
	case 4:
		startAddr = m_registers.control[4] ? 0x1000 : 0x0000;
		patternLowLatch = m_bus.videoRead(startAddr + ((uint16_t)nametableLatch * 16) + m_registers.videoAddress.fineY.to_ulong());
		break;
	case 6:
		startAddr = m_registers.control[4] ? 0x1000 : 0x0000;
		patternHighLatch = m_bus.videoRead(startAddr + ((uint16_t)nametableLatch * 16) + m_registers.videoAddress.fineY.to_ulong() + 8); // Just an offset of 8.
		break;
	case 7: // Overlaps with the previous operation and increments the horizontal part of videoAddress
		incrementHorizontalAddress();
		break;
	}
}

void PPU::exec()
{
	if (m_scanline == PPU_PRE_RENDER_SCANLINE)
	{
		if (m_scanlineCycle == 1)
		{
			m_registers.status = 0; // We reset everything in the status
		}
		else if (m_scanlineCycle >= PPU_COPY_HORIZONTAL_BEGIN && m_scanlineCycle <= PPU_COPY_HORIZONTAL_END)
		{
			copyVerticalAddress();
		}
	}
	if (renderingEnabled() && (m_scanline <= PPU_SCANLINE_VISIBLE_END || m_scanline == PPU_PRE_RENDER_SCANLINE))
	{
		if (m_registers.mask[3])
		{
			if ((m_scanlineCycle >= PPU_BACKGROUND_FETCH_BEGIN && m_scanlineCycle <= PPU_BACKGROUND_FETCH_END) || (m_scanlineCycle >= PPU_NEXT_FETCH_BEGIN && m_scanlineCycle <= PPU_NEXT_FETCH_END))
			{
				backgroundFetch();
			}
			if (m_scanline <= PPU_SCANLINE_VISIBLE_END && m_scanlineCycle >= PPU_PIXEL_DRAW_BEGIN && m_scanlineCycle <= PPU_PIXEL_DRAW_END && (m_registers.mask[1] || !m_registers.mask[1] && m_scanlineCycle > 8))
			{
				handlePixel();
			}
			if (m_scanlineCycle == PPU_BACKGROUND_FETCH_END)
			{
				incrementVerticalAddress();
			}
			else if (m_scanlineCycle == PPU_PAST_BACKGROUND_FETCH)
			{
				loadBackgroundShiftRegisters();
				copyHorizontalAddress();
			}
			else if (m_scanlineCycle == PPU_SCANLINE_CYCLES - 1 && oddFrame)
			{
				m_scanlineCycle = PPU_SCANLINE_CYCLES;
			}
		}
		if (m_registers.mask[4])
		{
			evaluateSprites();
		}

	}
	if (m_scanline == PPU_SCANLINE_VBLANK_LINE && m_scanlineCycle == 1)
	{
		render();
		if (m_registers.control[7])
		{
			interruptFlags::NMI = true;
		}
		m_registers.status[7] = true;
	}
	// Scanline incrementation.
	if (m_scanlineCycle == PPU_SCANLINE_CYCLES)
	{
		m_scanlineCycle = 0;
		if (m_scanline == PPU_PRE_RENDER_SCANLINE)
		{
			m_scanline = 0;
		}
		else
		{
			m_scanline++;
		}
		if (renderingEnabled())
		{
			oddFrame = !oddFrame;
		}
		else
		{
			oddFrame = false;
		}
	}
	else
	{
		m_scanlineCycle++;
	}
}

void PPU::evaluateSprites()
{
	if (!m_scanlineCycle)
	{
		secondaryOAMSize = 0;
		spriteZeroInUse = spriteZeroNextScanline;
		spriteZeroNextScanline = false;
	}
	else if (m_scanlineCycle >= PPU_SECONDARY_OAM_INITIALIZE_BEGIN && m_scanlineCycle <= PPU_SECONDARY_OAM_INITIALIZE_END)
	{
		initializeSecondaryOAM();
	}
	else if (m_scanlineCycle >= PPU_ACTUAL_EVALUATION_BEGIN && m_scanlineCycle <= PPU_ACTUAL_EVALUATION_END && !(m_scanlineCycle & 0x01))
	{
		actualSpriteEvaluation();
	}
	else if (m_scanlineCycle >= PPU_SPRITE_FETCH_BEGIN && m_scanlineCycle <= PPU_SPRITE_FETCH_END && (m_scanlineCycle % 8) == 0)
	{
		fetchSprites();
	}
}

void PPU::initializeSecondaryOAM()
{
	unsigned int n = (m_scanlineCycle - 1) / 2;
	switch (n % 4)
	{
	case 0:
		secondaryOAM[n / 4].yPosition = 0xFF;
		break;
	case 1:
		secondaryOAM[n / 4].tileIndex = 0xFF;
		break;
	case 2:
		secondaryOAM[n / 4].objectAttributes = 0xFF;
		break;
	case 3:
		secondaryOAM[n / 4].xPosition = 0xFF;
		break;
	}
}

void PPU::actualSpriteEvaluation()
{
	if (secondaryOAMSize < SECONDARY_OAM_SPRITE_CAPACITY)
	{
		unsigned int n = (m_scanlineCycle - 65) / 2;
		
		if (m_scanline >= primaryOAM[n].yPosition.to_ulong() && m_scanline < ((uint16_t)primaryOAM[n].yPosition.to_ulong() + (m_registers.control[PPU_SPRITE_SIZE] ? PPU_SPRITE_LARGE : PPU_SPRITE_SMALL)))
		{
			if (n == 0)
			{
				spriteZeroNextScanline = true;
			}
			secondaryOAM[secondaryOAMSize++] = primaryOAM[n];
		}
	}
}

void PPU::fetchSprites()
{
	unsigned int n = (m_scanlineCycle - 257) / 8;

	if (n >= secondaryOAMSize)
	{
		spriteX[n] = 0;
		spriteAttributes[n] = 0;
		spritePatternLow[n].loadEight(0);
		spritePatternHigh[n].loadEight(0);
	}
	else
	{
		uint16_t addr;
		if (m_registers.control[PPU_SPRITE_SIZE])
		{
			addr = ((secondaryOAM[n].tileIndex.to_ulong() & 0x01) * 0x1000) + ((secondaryOAM[n].tileIndex.to_ulong() & ~0x01) * 16);
		}
		else
		{
			addr = (m_registers.control[3] * 0x1000) + (secondaryOAM[n].tileIndex.to_ulong() * 16);
		}
		unsigned int sprY = (m_scanline - secondaryOAM[n].yPosition.to_ulong()) % (m_registers.control[PPU_SPRITE_SIZE] ? PPU_SPRITE_LARGE : PPU_SPRITE_SMALL);
		if (secondaryOAM[n].objectAttributes[7])
		{
			sprY ^= (m_registers.control[PPU_SPRITE_SIZE] ? PPU_SPRITE_LARGE - 1 : PPU_SPRITE_SMALL - 1);
		}
		addr += sprY + (sprY & 8);
		spriteX[n] = secondaryOAM[n].xPosition;
		spriteAttributes[n] = secondaryOAM[n].objectAttributes;
		spritePatternLow[n].loadEight(m_bus.videoRead(addr));
		spritePatternHigh[n].loadEight(m_bus.videoRead(addr | 0x08));

		if (secondaryOAM[n].objectAttributes[OAM_FLIP_X])
		{
			spritePatternLow[n].flipValue();
			spritePatternHigh[n].flipValue();
		}
	}
}