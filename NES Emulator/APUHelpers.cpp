#include "APUHelpers.h"

LengthCounter::LengthCounter() : m_enabled(), m_halt(), m_counter()
{
}

void LengthCounter::setHalt(bool halt)
{
	m_halt = halt;
}

void LengthCounter::setEnabled(bool enabled)
{
	m_enabled = enabled;
	if (!m_enabled)
	{
		m_counter = 0;
	}
}

void LengthCounter::setCounter(size_t index)
{
	if (m_enabled)
	{
		m_counter = m_lookupTable[index];
	}
}

size_t LengthCounter::getCounter()
{
	return m_counter;
}

bool LengthCounter::disableChannel()
{
	return !m_counter;
}

void LengthCounter::clock()
{
	if (!m_halt && m_counter)
	{
		m_counter--;
	}
}

Divider::Divider() : m_period(0), m_counter(0)
{}

size_t Divider::getPeriod()
{
	return m_period;
}

size_t Divider::getCounter()
{
	return m_counter;
}

void Divider::setPeriod(size_t period)
{
	m_period = period;
}

void Divider::resetCounter()
{
	m_counter = m_period;
}

bool Divider::clock()
{
	if (!m_counter)
	{
		resetCounter();
		return true;
	}
	m_counter--;
	return false;
}

Envelope::Envelope() : m_restart(true), m_counterLoopEnabled(false), m_counter(0), m_constantVolumeEnabled(false), m_constantVolume(0)
{}

void Envelope::restartEnvelope()
{
	m_restart = true; // Restarting the envelope is handled by the clock function.
}

void Envelope::setCounterLoopEnabled(bool enable)
{
	m_counterLoopEnabled = enable;
}

void Envelope::setConstantVolumeEnabled(bool enable)
{
	m_constantVolumeEnabled = enable;
}

void Envelope::setConstantVolume(size_t volume)
{
	m_constantVolume = volume;
	m_divider.setPeriod(m_constantVolume);
}

size_t Envelope::getVolume()
{
	if (m_constantVolumeEnabled)
	{
		return m_constantVolume;
	}
	return m_counter;
}

void Envelope::clock()
{
	if (m_restart)
	{
		m_restart = false;
		m_counter = 15;
		m_divider.resetCounter();
	}
	else
	{
		if (m_divider.clock())
		{
			if (m_counter)
			{
				m_counter--;
			}
			else if (m_counterLoopEnabled) // Counter has reached 0, but looping is enabled.
			{
				m_counter = 15;
			}
		}
	}
}

Timer::Timer() : m_divider(), m_minimumPeriod(0)
{}

void Timer::reset()
{
	m_divider.resetCounter();
}

size_t Timer::getDividerPeriod()
{
	return m_divider.getPeriod();
}

void Timer::setDividerPeriod(size_t period)
{
	m_divider.setPeriod(period);
}

void Timer::setLowBitsOfDividerPeriod(size_t value)
{
	m_divider.setPeriod((m_divider.getPeriod() & 0x700) | value);
}

void Timer::setHighBitsOfDividerPeriod(size_t value)
{
	m_divider.setPeriod((value << 8) | (m_divider.getPeriod() & 0xFF));
	m_divider.resetCounter();
}

void Timer::setMinimumPeriod(size_t period)
{
	m_minimumPeriod = period;
}

bool Timer::clock()
{
	if (m_divider.getPeriod() < m_minimumPeriod)
	{
		return false;
	}
	return m_divider.clock();
}

void SweepUnit::calculateTargetPeriod(Timer& timer)
{
	uint16_t period = timer.getDividerPeriod();
	uint16_t shiftedPeriod = period >> m_shiftAmount;
	if (m_negate)
	{
		m_targetPeriod = period - (shiftedPeriod - (m_adjustForPulseOne ? 1 : 0));
	}
	else
	{
		m_targetPeriod = period + shiftedPeriod;
	}
	m_disableChannel = (period < 8 || m_targetPeriod > 0x7FF);
}

void SweepUnit::adjustTimerDividerPeriod(Timer& timer)
{
	if (m_enabled && m_shiftAmount && !m_disableChannel)
	{
		timer.setDividerPeriod(m_targetPeriod);
	}
}

SweepUnit::SweepUnit(bool pulseOne) : m_adjustForPulseOne(pulseOne), m_enabled(), m_negate(), m_reload(), m_disableChannel(), m_shiftAmount(0), m_divider(), m_targetPeriod(0)
{}

void SweepUnit::setEnabled(bool enabled)
{
	m_enabled = enabled;
}

void SweepUnit::setNegate(bool negate)
{
	m_negate = negate;
}

void SweepUnit::setDividerPeriod(size_t period, Timer& timer)
{
	m_divider.setPeriod(period);
	calculateTargetPeriod(timer);
}

void SweepUnit::setShiftAmount(size_t amount)
{
	m_shiftAmount = amount;
}

void SweepUnit::reload()
{
	m_reload = true;
}

bool SweepUnit::channelDisabled()
{
	return m_disableChannel;
}

void SweepUnit::clock(Timer& timer)
{
	calculateTargetPeriod(timer);
	
	if (m_reload)
	{
		m_reload = false;
		if (m_enabled && m_divider.clock())
		{
			adjustTimerDividerPeriod(timer);
		}
		m_divider.resetCounter();
	}
	else
	{
		if (m_divider.getCounter())
		{
			m_divider.clock();
		}
		else if (m_enabled && m_divider.clock())
		{
			adjustTimerDividerPeriod(timer);
		}
	}
}

LinearCounter::LinearCounter() : m_reload(true), m_control(true), m_divider()
{}

void LinearCounter::reload()
{
	m_reload = true;
}

void LinearCounter::handleWrite(size_t value)
{
	m_control = value >> 7;
	m_divider.setPeriod(value & 0x7F); // Low 7 bits.
}

void LinearCounter::clock()
{
	if (m_reload)
	{
		m_divider.resetCounter();
	}
	else if (m_divider.getCounter())
	{
		m_divider.clock();
	}

	if (!m_control)
	{
		m_reload = false;
	}
}

size_t LinearCounter::getDividerCounter()
{
	return m_divider.getCounter();
}

bool LinearCounter::disableChannel()
{
	return !m_divider.getCounter();
}

PulseWaveGenerator::PulseWaveGenerator() : m_duty(0), m_step(0)
{}

void PulseWaveGenerator::restart()
{
	m_step = 0;
}

void PulseWaveGenerator::setDuty(size_t duty)
{
	m_duty = duty;
}

size_t PulseWaveGenerator::getValue()
{
	return m_sequenceValues[m_duty][m_step];
}

void PulseWaveGenerator::clock()
{
	m_step = (m_step + 1) % 8;
}

TriangleWaveGenerator::TriangleWaveGenerator() : m_step(0)
{}

size_t TriangleWaveGenerator::getValue()
{
	return m_sequence[m_step];
}

void TriangleWaveGenerator::clock()
{
	m_step = (m_step + 1) % 32;
}

LinearFeedbackShiftRegister::LinearFeedbackShiftRegister() : m_register(1), m_mode(false)
{}

void LinearFeedbackShiftRegister::setMode(bool mode)
{
	m_mode = mode;
}

bool LinearFeedbackShiftRegister::disableChannel()
{
	return m_register & 0x1;
}

void LinearFeedbackShiftRegister::clock()
{
	size_t bitZero = m_register & 0x1;
	size_t xorBit = (m_register >> (m_mode ? 5 : 1)) & 0x1;
	size_t feedback = bitZero ^ xorBit;

	m_register = (m_register >> 1) | (feedback << 14);
}
