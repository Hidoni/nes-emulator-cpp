#include "Cartridge.h"
#include "CartridgeException.h"
#include <fstream>

Cartridge::Cartridge(std::string romPath)
{
	std::ifstream rom(romPath, std::ios::binary);
	if (rom.is_open())
	{
		// Read rom into a vecotr of bytes
		std::vector<uint8_t> romdata = std::vector<uint8_t>(std::istreambuf_iterator<char>(rom), std::istreambuf_iterator<char>());
		rom.close();
		if (validateFile(romdata))
		{
			m_mapper = new Mapper(romdata);
			loadedMapper = true;
			if (romdata[6] & 0x1)
			{
				mirroringType = mirroringTypes::vertical;
			}
			else
			{
				mirroringType = mirroringTypes::horizontal;
			}
		}
		else
		{
			throw CartridgeException("Invalid Header For File!");
		}
	}
	else
	{
		throw CartridgeException("File could not be opened.");
	}
}

Cartridge::~Cartridge()
{
	if (loadedMapper)
	{
		delete m_mapper;
	}
}

Mapper* Cartridge::getMapper()
{
	return m_mapper;
}

inline bool Cartridge::validateFile(std::vector<uint8_t> data)
{
	return (data[0] == 'N' && data[1] == 'E' && data[2] == 'S' && data[3] == 0x1A); // iNES header **must** start with these 4 values, thus we can compare them to verify the file is an actual ROM.
}