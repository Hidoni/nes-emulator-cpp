#pragma once
#include "APUHelpers.h"
#include <iostream>
#include <bitset>
#include <vector>

#define GET_THREE_LOW_BITS 0x7
#define GET_FIVE_HIGH_BITS 0xF8
#define GET_TWO_HIGH_BITS 0xC0
#define GET_BIT_FIVE 5
#define GET_BIT_FOUR 4
#define GET_FOUR_LOW_BITS 0xF
#define GET_BIT_SEVEN 7
#define GET_BIT_THREE 3
#define GET_BITS_SIX_TO_FOUR 0x70
#define GET_SEVEN_LOW_BITS 0x7F
#define GET_BIT_SIX 6
#define GET_BIT_TWO 2
#define GET_BIT_ONE 1
#define GET_BIT_ZERO 0
#define GET_FOUR_HIGH_BITS 0xF0

struct APUChannel
{
	LengthCounter m_lengthCounter;
	Timer m_timer;
public:
	LengthCounter& getLengthCounter();
};

class PulseChannel : public APUChannel
{
private:
	Envelope m_envelope;
	SweepUnit m_sweepUnit;
	PulseWaveGenerator m_generator;
public:
	PulseChannel(bool isFirst);
	void handleQuarterClock();
	void handleHalfClock();
	void clockTimer();
	void handleCPUWrite(uint16_t addr, uint8_t value);
	uint8_t getValue();
};

class TriangleChannel : public APUChannel
{
private:
	LinearCounter m_linearCounter;
	TriangleWaveGenerator m_generator;
public:
	TriangleChannel();
	void handleQuarterClock();
	void handleHalfClock();
	void clockTimer();
	void handleCPUWrite(uint16_t addr, uint8_t value);
	uint8_t getValue();
};

class NoiseChannel : public APUChannel
{
private:
	void setTimerPeriod(uint8_t lookupIndex);
	Envelope m_envelope;
	LinearFeedbackShiftRegister m_register;

public:
	NoiseChannel();
	void handleQuarterClock();
	void handleHalfClock();
	void clockTimer();
	void handleCPUWrite(uint16_t addr, uint8_t value);
	uint8_t getValue();
};

class DMCChannel
{
	// We need to figure this out.
};